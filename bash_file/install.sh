pip3 install spacy treelib django flask pandas neuralcoref

python3 -m spacy download en_core_web_md 
python3 -m spacy download en
python3 -m spacy download en_core_web_lg

cd ./src/front_end/
npm install


cd ../../src/api/
mkdir out

cd out
mkdir single
mkdir multi_split
mkdir multi
mkdir view_actor
mkdir view_boundary
mkdir view_entity