from django.urls import path
from . import views

urlpatterns = [
	path("basic/", views.basic, name="basic"),
	path("full", views.full, name="full"),
	path("image/", views.image, name="image"),
	path("generate/", views.generate, name="generate"),
	path("multi/", views.generate_multi, name="multi"),
	path("log/", views.log, name="log")
]
