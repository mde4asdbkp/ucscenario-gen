import os
from datetime import datetime
from treelib import Tree as PrintTree


def pretty_print(tree, text_func=lambda n: n.word, child_func=lambda n: n.modifiers):
    """
    Prints a tree from the given list of root nodes. Note that nodes must be unique.
    :param tree: Root nodes for the tree.
    :param text_func: lambda function for the text to display in the tree.
    :param child_func: lambda function for getting the children of each node.
    :return: None
    """
    print_tree = PrintTree()
    print_tree.create_node("ROOT", "ROOT")
    count = 0
    for node in tree:
        node_id = str(node) + node.__repr__()
        count += 1
        print_tree.create_node(text_func(node), node_id, "ROOT", count)
        _add_node_to_print_tree(
            node, print_tree, count,
            child_func=child_func,
            text_func=text_func,
            id_func=lambda x: str(x) + str(x.__repr__())
        )
    print_tree.show(key=lambda n: n.data)


def _add_node_to_print_tree(node, print_tree, count, child_func, text_func, id_func):
    for c in child_func(node):
        count += 1
        print_tree.create_node(text_func(c), id_func(c), id_func(node), data=count)
        _add_node_to_print_tree(c, print_tree, count, child_func, text_func, id_func)


class Logger:

    def __init__(self, title: str=None):
        self._categories = []
        self._logs = []
        self._title = title

    def log(self, category: str, log: str):
        self._categories.append(category)
        self._logs.append(log)

    def __str__(self):
        log = "#" * 80 + "\n"

        if self._title:
            log += self._title.upper() + "\n" + "-" * 80

        log += "\n\n"

        for i in range(len(self._logs)):
            log += self._categories[i] + ":\n"
            log += self._logs[i] + "\n\n"

        log += "#" * 80
        return log

    def print_to_file(self, location: str, file_name: str=None) -> str:
        if not file_name:
            file_name = str(datetime.now())
        log_file = open(os.path.join(location, file_name), "w+")
        log_file.write(str(self))
        log_file.close()
        return str(self)


__all__ = ["pretty_print", "Logger"]
