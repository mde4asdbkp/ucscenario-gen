from abc import ABC, abstractmethod
from typing import List, Tuple, Optional, Set, Dict
from copy import copy
from math import floor
from enum import Enum, auto
from importlib.util import find_spec

from spacy.lang.en.stop_words import STOP_WORDS

use_src = find_spec("src")

if use_src:
    from src.storyparser.entity import Entity, EntityType, Composite, RelationOnlyComposite, Property
    from src.storyparser.node import Node
    from src.storyparser.nlp_enums import Arc, POSFine
else:
    from storyparser.entity import Entity, EntityType, Composite, RelationOnlyComposite, Property
    from storyparser.node import Node
    from storyparser.nlp_enums import Arc, POSFine


class Flag(Enum):
    conjunction_and = auto()
    conjunction_or = auto()
    horizontal_stack = auto()
    statement = auto()
    relation = auto()
    interface = auto()


class Carryover:
    """
    Contains a list of flags that are gathered while processing rules. These flags are for use in later checks. For
    example a conjunction (e.g. and/or) will effect future conjuncts differently. For example an and means the 
    conjunction of adjectives (A fast and reliable developer => Fast, Reliable Developer) while an or means duplicate
    and append (A fast or reliable developer => Fast Developer & Reliable Developer)
    """

    def __init__(self):
        self._flags: List[Flag] = []
        self.actor_reference: Entity = None
        self.pobj_reference: Entity = None
        self.dobj_reference: Entity = None
        self.references: Dict[Arc, Entity] = {}

    def check_and_consume(self, flag: Flag) -> bool:
        """
        Checks for a flag and consumes it if present
        :param flag: Flag to check for.
        :return: True if present and consumed, false otherwise
        """
        present = flag in self._flags
        if present:
            self._flags.remove(flag)
        return present

    def check(self, flag: Flag) -> bool:
        return flag in self._flags

    def add_flag(self, flag: Flag):
        self._flags.append(flag)

    def clear_flags(self) -> "Carryover":
        self._flags = []
        return self

    def update_references(self, source: Node, entity: Entity, combine: bool=False):
        if source.arc == Arc.conj:
            p = source.parent
            while p and p.arc == Arc.conj:
                p = p.parent
            self.update_references(p, entity, combine=True)
            return
        etype = entity.entity_type
        if entity.entity_type == EntityType.composite:
            etype = entity.children[-1].entity_type
        if etype == EntityType.actor:
            if combine:
                compound, _ = stack(self.pobj_reference, entity, vertical=False)
                self.actor_reference = compound
            else:
                self.actor_reference = entity
        elif etype == EntityType.object:
            if combine:
                compound, _ = stack(self.pobj_reference, entity, vertical=False)
                self.references[source.arc] = entity
            else:
                self.references[source.arc] = entity


def strip(entity: Entity):
    while entity.pretext:
        entity.remove_pretext()
    while entity.description:
        entity.remove_description()
    entity.case = ""
    while entity.post:
        entity.remove_post()
    return entity


def stack(parent: Optional[Entity], child: Entity,
          vertical: bool=True, create_compound: bool=True, relation_only: bool=False) -> Tuple[Entity, List[Entity]]:
    """
    Si il y a un parent:
        si on a une relation vertical : on ajoute une relation parent vers child
        sinon si create_compound : créé un RelationOnlyComposite si relation_only sinon Composite à partir de parent et child
                sinon proxys = child 
        on renvoie child ou le child_proxy

    If vertical :
        P <- C
    """
    if parent:
        new_entities = []
        if vertical:
            proxy, copies = parent.add_relation(child)
            print("stack : verticale ---")
            print(copies)
            #print(proxy)
            new_entities += copies
        else:
            if create_compound:
                pc = [parent, child]
                proxy = RelationOnlyComposite.build_from(pc) if relation_only else Composite.build_from(pc)
                new_entities.append(proxy)
            else:
                proxy = child

            print("stack : horizontal ---")
        print(proxy)
        return proxy, new_entities
    return child, []

def insert_stack(parent: Optional[Entity], child: Entity) -> Tuple[Entity, List[Entity]]:
    """
    Insert the child entity between that his parent and his grand-parent (parent of parent):
        before : GP <= P 
        after : GP <= C <= P

    return child with new parent and new_ents from stack
    """
    if(parent):
        gparent = parent.parent

        if(gparent):
            updated, new_ents = stack(gparent,child)

        updated, new_ents = stack(child,parent)
        return child, new_ents
    else:
        return child, []



class ParseRule(ABC):

    @abstractmethod
    def check_conditions(node: Node) -> bool:
        """
        Returns true if the given node meets the conditions to be be parsed by this rule. A node should technically only
        meet one or no rules.
        :param node: The node to check against the rules conditions.
        :return: If the node passes the rule or not.
        """
        pass

    @abstractmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        """
        Parses a node according to some given rules. The return values from parse methods are intended to be fed into
        subsequent calls to parse() for future nodes.
        :param node: Node The node to parse
        :param default_type: The default type for an Entity if it is created. This is not always used but in general is
                             used to determine the difference between ambiguous "things" (is this a person or an object)
        :param explicit_entities: True if Entities should be labeled as explicit, in other words do nouns represents
                                  things by default or are the words in a larger part of a sentence?
        :param carryover: Any flags set by previous calls to parse() using the same CarryoverFlag instance.
        :return: In order:
                    Optional[Entity]: The entity this node represents if it exists.
                    List[Operation]: Any operations intended for execution on the last created entity.
                    bool: This flag is False if a returned Entity should be stacked horizontally with the last or True
                        otherwise. Horizontal stacking means it should have the same parents and operations applied to
                        it as the last Entity. This is intended to be represented by EntityType.composite and
                        a non-empty Entity.children.
                    CarryoverFlags: Any unconsumed or new flags gathered by parsing this node.
                    List[Node]: A list of all the nodes to search next. Usually just the modifiers of the parsed node.
        """
        pass


class AdjectivalComplement(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.acomp and \
               (any(entity.arc in Arc.all_obj() + [Arc.prep] for entity in node.modifiers) or not node.modifiers)

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)#, explicit=explicit_entities)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class AdverbialClausalModifier(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.advcl

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:

        #mark is like if, when(advmod), that, ...
        marks = [mod for mod in node.modifiers if(mod.arc in [Arc.mark, Arc.advmod]  )]
        #if we have a subject or a expl like "there"
        subj = [mod for mod in node.modifiers if (mod.arc in [Arc.expl,Arc.nsubj,Arc.nsubjpass])]
        first_p_subj = [mod for mod in subj if(mod.word.lower()  in ["i","we"])]

        if(node.pos_fine in [POSFine.VB]):
            entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)
            updated, new_ents = stack(parent,entity)

        elif(node.pos_fine in [POSFine.VBP,POSFine.VBN]):
            if(len(marks)>0 and len(first_p_subj)>0):
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)
                updated, new_ents = insert_stack(parent, entity)

            #Ex: As a moderator, I want to add an item to the list of items to be estimated
            elif(len([mod for mod in node.modifiers if(mod.word.lower()  in ["to"])])>0):
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)
                updated, new_ents = stack(parent,entity)
                
            elif(len(first_p_subj)==0):
                #exple : I want to create a profile if there are 3 others profiles
                #there isn't first pers so we do nothing and we stop tree parsing in this branch
                #TODO case don't use actually maybe in the futur
                return None , [] , carryover, []
        elif(node.pos_fine in [POSFine.VBG]):
            entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
            updated, new_ents = stack(parent,entity)

        else:
            #All the other case are not use we stop the tree parsing.
            #TODO case don't use actually maybe in the futur
            return None , [] , carryover, []
            
        return updated, [entity] + new_ents, carryover, node.modifiers


class Attribute(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.attr

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension, explicit=explicit_entities)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class Case(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.case

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        if parent:
            parent.case = node.word
        return parent, [], carryover, node.modifiers


class ClausalComplement(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        # same acomp check as for Open Clausal Complement
        if node.arc != Arc.ccomp:
            return False
        has_acomp = False
        for mod in node.modifiers:
            has_acomp = has_acomp or mod.arc == Arc.acomp
        return not has_acomp

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:

        #mark is like if, when(advmod), that, ...
        marks = [mod for mod in node.modifiers if(mod.arc in [Arc.mark, Arc.advmod]  )]
        #if we have a subject or a expl like "there"
        subj = [mod for mod in node.modifiers if (mod.arc in [Arc.expl,Arc.nsubj,Arc.nsubjpass])]
        first_p_subj = [mod for mod in subj if(mod.word.lower()  in ["i","we"])]
       
        if (node.pos_fine in [POSFine.VBN,POSFine.VB,POSFine.VBZ,POSFine.VBP,POSFine.VBD,POSFine.VBG]):
            if(len(first_p_subj)>0 and len(marks)>0):
                #Ex: As a researcher, I want to see descriptive metadata for the item whether I come to the item through the repository
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)
                updated, new_ents = insert_stack(parent, entity)
                return updated, [entity] + new_ents, carryover, node.modifiers

            elif(any([mod for mod in node.modifiers if(mod.word.lower()  in ["to"])])):
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)

                #Ex: As a user, I want the publish button in FABS to deactivate a page.
                # Publish button is an Interface and the subj of deactivate
                if any([mod for mod in node.modifiers if mod._arc in Arc.all_subj()]):
                    carryover.add_flag(Flag.interface)
                search_parent = parent 
                while(search_parent and search_parent.entity_type != EntityType.relation):
                    search_parent = search_parent.parent

                if(search_parent and search_parent.entity_type == EntityType.relation):
                    parent = search_parent
                
                updated, new_ents = stack(parent, entity)
                return updated, [entity] + new_ents, carryover, node.modifiers
            else : 

                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
                updated, new_ents = insert_stack(parent, entity)
                return updated, [entity] + new_ents, carryover, node.modifiers
        
        
        
        return parent, [], carryover, node.modifiers


class ClausalModifier(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.acl and any(mod.arc in Arc.all_obj() for mod in node.modifiers)

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        #TODO REMOVE
        #if not carryover.check(Flag.statement):
        #   carryover.add_flag(Flag.relation)
        entity = Entity(node.lemma, node.lemma, node.index, entity_type=EntityType.relation)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class Compound(ParseRule):
    """
    Compound statements are multiple things listed in order to create a new thing. For example
    ['airplane', 'flight', 'control', 'system']. Each term is interpreted as a property of the one that comes
    before it.
    """

    @staticmethod
    def check_conditions(node: Node):    
        return node.arc == Arc.compound

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        if parent:
            # no reason to assume nested actor or actor properties.
            if parent.entity_type == EntityType.actor:
                parent.add_description(node.word, prepend=node.parent.arc == Arc.compound)
                return parent, [], carryover, node.modifiers
            # assuming boundary
            possible_relation = parent.parent
            while possible_relation and possible_relation.entity_type == EntityType.extension:
                possible_relation = possible_relation.parent

            if possible_relation and possible_relation.lemma == node.lemma:
                parent.add_description(node.word, prepend=False)
                parent.entity_type = EntityType.interface
                return parent, [], carryover, node.modifiers

            # assuming property
            elif parent.properties:
                for prop in parent.properties:
                    prop.append(Property(node.word))
            else:
                parent.add_property(Property(node.word))
                return parent, [], carryover, node.modifiers
        return parent, [], carryover, node.modifiers


class Conjunct(ParseRule):
    """
    Conjuncts are the word that follows are conjunction (conjunctions being and, or etc...) These are especially
    annoying because they force you to track multiple items in a composite object. Also because they can have
    any entity type but all have the same dependency so information is obscured.
    """

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.conj or node.arc == Arc.appos

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        

        if not parent:
            return parent, [], carryover, node.modifiers
        
        gp = parent.parent 
        if gp :

            """
            We are looking for an entity relation in our parent's ancestors. 
            We duplicate this by removing our parent who corresponds to the other element of the conjunction to allow the creation of another action flow.
            If we do not find an ancestor who is an entity.relation, we use the grandfather.
            """
            dup_gp , new_dup_ents = gp.duplicate(copy_up=True,include_children=False,convert_type=False,ignore=[parent])
            dup_gp.referenced_explicitly = True

            while gp and gp.entity_type != EntityType.relation:
                gp = gp.parent

            if(gp):
                dup_gp , new_dup_ents = gp.duplicate(copy_up=True,include_children=False,ignore=[parent])
                dup_gp.referenced_explicitly = True


            if(node.pos_coarse == "VERB"):
                entity = Entity(node.lemma, node.lemma, node.index, entity_type=EntityType.relation)
            else:
                entity = Entity(node.lemma, node.lemma, node.index, entity_type=EntityType.object)

            updated, new_ents = stack(dup_gp, entity)
            
            return updated, [entity] + new_ents + new_dup_ents, carryover, node.modifiers

         ##OLD CODE maybe useful in some case when we don t have a grand parent
        
        # todo | it's highly probably that Conjuncts will work better if the code checks the type of parent rather than
        # todo | POS or similar.
        vertical = not carryover.check_and_consume(Flag.horizontal_stack)
        is_or = carryover.check_and_consume(Flag.conjunction_or)
        is_and = carryover.check_and_consume(Flag.conjunction_and)
        compound, relation_only = True, False

        entity, new = parent, []

        # in some cases when a verb/adjective can be a noun (e.g. the drunk vs drunk man vs I drunk water) the word
        # gets tagged incorrectly.
        parent_is_noun = node.parent.arc == Arc.conj and node.parent.pos_fine in POSFine.all_noun()
        print("pos fine = ",node.pos_fine)
        if node.pos_fine in POSFine.all_noun() or parent_is_noun:
            # fine to add new actors to the same control.
            print("on print ici")
            if parent.is_actor():
                entity, new = parent.duplicate(dup_down=False, copy_up=True, dup_up=False)
                entity.index = node.index
                # compound = False

            ##TODO Test FIX BUG
            elif(parent.entity_type == EntityType.relation):
                pass   
            else:
                entity, new = parent.duplicate(dup_down=False, dup_up=True, include_properties=False, convert_type=False)
                entity.entity_type = EntityType.object
                entity.index = node.index
                # entity, new = parent.duplicate(dup_down=False, copy_up=True, dup_up=False)
            strip(entity)
            entity._pretext = []
            entity.root = node.word
            entity.lemma = node.lemma
            carryover.update_references(node, entity, combine=True)

            relation_only = True
            compound = parent.entity_type not in [EntityType.relation, EntityType.statement]

        elif node.pos_fine in [POSFine.JJ, POSFine.VBN] or node.parent.arc in Arc.all_mod():
            if is_or:
                entity, new = parent.duplicate()
                entity.remove_description()

            entity.add_description(node.word)
            compound = False

        elif node.pos_fine in [POSFine.VB, POSFine.VBP]:
            # in the case of pcomp linguistic relations are (r1) --> (stuff) & (r1) --> (r2) rather than the standard
            # (r1) --> (r2) ---> (stuff) so everything needs copies over.
            p = node.parent
            while p.parent and p.parent.arc == Arc.conj:
                p = p.parent

            # same acomp check as normal.
            if node.modifiers and any(mod.arc == Arc.acomp for mod in node.modifiers):
                return parent, [], carryover, node.modifiers
            
            print(" on est ici en conj :")
            if(parent.entity_type == EntityType.composite):
                print("type enfant : ",parent.children[-1].referenced_explicitly)
                if(parent.children):
                    entity, new = parent.children[-1].duplicate(dup_down=p.arc in [Arc.pcomp])
                    #entity = Entity(node.word, node.lemma, entity_type=parent.children[-1].entity_type, explicit=parent.children[-1].referenced_explicitly)
                else:
                    entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation, explicit=True)

            else:
                entity, new = parent.duplicate(dup_down=p.arc in [Arc.pcomp])


            entity.print()

            entity.root = node.lemma if (not any(c.arc == Arc.compound for c in node.modifiers)) else node.word
            entity.lemma = node.lemma
            compound = all(r.entity_type == EntityType.extension for r in parent.relations)
            relation_only = True

        else:
            entity.add_description(node.word, prepend=False)

        # parent == entity if it was a description change.
        if parent is not entity:
            entity, new_ents = stack(parent, entity,
                                     vertical=vertical, create_compound=compound, relation_only=relation_only)
            new += new_ents
       
        ## TODO Test Case VB and VB but Spacy Miss order relation
        """
        if(parent.entity_type == EntityType.relation):
            if(node.any_modifiers_pos_fine("VB")):
                entity, new_ents = stack(parent, entity,
                                     vertical=True, create_compound=compound, relation_only=relation_only)
                new += new_ents
        """
        return entity, new, carryover, node.modifiers


class PrepositionComplement(ParseRule):
    """
    Prepositional Complements are processed all at once because their structure is a little awkward. The prepositional
    complement is like other complements in that it denotes a relation but because it follows a preposition it's
    slightly different syntactically. e.g.

    "View data on people." Is an object (data) followed by a preposition (on) and people. The relation here is an
    open clausal complement and has no relevant parents.

    Prepositional complement is object of the preposition that completes the meaning of sentence

    For example - The book is on the table.

    “On” is the preposition and “ the table ”is its complement.

    We were able to observe two different cases, 
    the one where the Parent is a Relation and the node of type VBG.
    For example: "As an archivist, I want to search images by uploading an image"
    uploading is PrepositionComplement.
    We can see that there is a relationship that occurs before the parent.
    We want to :  (search)->(uploading)->(search's parent)

    For the other case, we only have additional information about the parent.
    We therefore create an Extension linked to the parent
    """

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.pcomp

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:

        if parent: 
            ancestor_rel = parent
            while ancestor_rel and ancestor_rel.entity_type != EntityType.relation:
                ancestor_rel = ancestor_rel.parent
            if(ancestor_rel and ancestor_rel.entity_type == EntityType.relation and node._pos_fine in [POSFine.VBG]):
                #Ex : As an archivist, I want to search images by uploading an image
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation, explicit=True)
                updated, new_ents = insert_stack(ancestor_rel, entity)

            else :
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension, explicit=True)
                updated, new_ents = stack(parent, entity)

            return updated, [entity] + new_ents, carryover, node.modifiers

    @staticmethod
    def dfs(n):
        if n.arc == Arc.pobj:
            return n
        else:
            for m in n.modifiers:
                r = PrepositionComplement.dfs(m)
                if r:
                    return r


class Conjunction(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc in [Arc.cc, Arc.preconj]

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        # todo consider conjunctions
        if node.word.lower() in ["or"]:
            carryover.add_flag(Flag.conjunction_or)
        else:
            carryover.add_flag(Flag.conjunction_and)
        carryover.add_flag(Flag.horizontal_stack)
        return parent, [], carryover, node.modifiers


class OpenClausalComplement(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        # acomp check is because sometimes open clausal complements have another complement acting as an adjective.
        # Think the verb "be" (which is a xcomp) and "be able" where "able" is the adjective to "be". I have never run
        # into a situation where an xcomp of this form is useful.
        if node.arc != Arc.xcomp:
            return False
        has_acomp = False
        for mod in node.modifiers:
            has_acomp = has_acomp or mod.arc == Arc.acomp
        return not has_acomp 

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        new = []
        #TODO REMOVE TEST
       # statement = carryover.check(Flag.statement)
       # if not statement:
           # carryover.add_flag(Flag.relation)
        explicit = explicit_entities #and not statement

        entity = Entity(node.word ,
                        node.lemma,
                        index=node.index,
                        entity_type=EntityType.relation,
                        explicit=explicit)
        # this is for when an xcomp (e.g. a control) is referred to as a "thing".
        # for example = "I want to view my organs to donate" has donate as an implicit control.
        
        
        updated, n = stack(parent, entity)
        new += n
        return updated, [entity] + new, carryover, node.modifiers


class PossessiveNominalModifier(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        # As the name suggests a Possessive Nominal Modifier is a modifier for a word that shows position. An example is
        # MY profile or HER car. Also if modified by a case ('s) then an object/subject becomes a poss. E.g. friend's
        # car. These checks are for those three conditions although as far as I am aware they are largely a hangover
        # from earlier iterations of the program where not all cases where handled.
        if node.arc != Arc.poss:
            return False
        if node.parent:
            if node.parent.arc in Arc.all_obj() or node.parent.arc in Arc.all_subj():
                return True
            for sibling in node.parent.modifiers:
                if sibling.arc == Arc.nummod:
                    return True
        for mod in node.modifiers:
            if mod.arc == Arc.case:
                return True
        return node.lemma == "-PRON-"

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:

        #stop word for cases like Whose which is not tagged as a pronoun but has the same function
        if node.lemma == "-PRON-" or node.lemma in STOP_WORDS:
            entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.primary_actor_proxy)
        else:
            entity = Entity(node.word, node.lemma, node.index, entity_type=default_type, explicit=explicit_entities)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents , carryover, node.modifiers


class GenericContext(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc in Arc.all_context() and node.pos_fine not in [POSFine.MD]

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        if parent and node in node.parent.modifiers:
            parent.add_pretext(node.word)
        return parent, [], carryover, node.modifiers


class GenericObject(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc in Arc.all_obj()

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        

        # A statement like "A or B combined with C" doesn't have "combined" as a child of B, it's A instead.
        # It's much easier to parse if it's the child of B so that it applied to both A and B via the composite
        # Entity created at B so this is changed here.
                
        print("mes parent :",parent)
        #OLD CODE REMOVE NEED TO TEST 
        """
        conjuncts = list(map(lambda m: Comma.check_conditions(m) or Conjunct.check_conditions(m), node.modifiers))
        index_of_conjunct = conjuncts.index(True) if True in conjuncts else None
    
        if index_of_conjunct and index_of_conjunct != len(node.modifiers):
            print("GEN CONJ OBJ")
            final_conjunction = node.modifiers[index_of_conjunct]
            while any(m.arc == Arc.conj for m in final_conjunction.modifiers):
                final_conjunction = [m for m in final_conjunction.modifiers if m.arc == Arc.conj][-1]
            final_conjunction.add_modifiers(node.modifiers[index_of_conjunct + 1:])
    """
        # Sometimes compound words are not stacked vertically, especially when more than 3+ stacked. This simply
        # ensures that they are nested so that properties works correctly.
        compounds = [mod for mod in node.modifiers if mod.arc == Arc.compound]
        if len(compounds) > 1:
            target = compounds[-1]
            while [c.arc == Arc.compound for c in target.modifiers]:
                target = [c for c in target.modifiers if c.arc == Arc.compound][-1]
            for i in range(0, len(compounds)-1, 1):
                target.add_modifier(compounds[i])

        entity = Entity(node.word, node.lemma, node.index,
                        entity_type=default_type,
                        explicit=explicit_entities)
        explore = node.modifiers

        # if using an object to access a relation.
        if any(ClausalModifier.check_conditions(n) for n in node.modifiers):
            entity.entity_type = EntityType.interface
            entity.referenced_explicitly = False

        is_prp = node.pos_fine in [POSFine.PRP, POSFine.PRP_dollar]
        if is_prp:
            if node.arc in carryover.references:
                entity, new = carryover.references[node.arc].duplicate(dup_up=False, dup_down=False)
            else:
                entity.entity_type = EntityType.object_proxy
        else:
            carryover.update_references(node, entity)

        if not is_prp and len(node.modifiers) >= 1 and node.modifiers[-1].arc in Arc.all_obj():
            n = node.modifiers[0]
            entity = Entity(n.word, n.lemma, index=node.index ,entity_type=default_type, explicit=explicit_entities)
            entity.add_description(node.word)
            explore = n.modifiers
        
        if parent:
            if(parent.root.lower() =="of" and parent.parent and parent.parent.entity_type in [EntityType.object, EntityType.object_proxy, EntityType.interface]): 
                parent.parent.is_property_object = True
        updated, new_ents = stack(parent, entity)

        return updated, [entity] + new_ents, carryover, explore


class GenericSubject(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc in Arc.all_subj() and node.pos_fine in POSFine.all_noun() + [POSFine.PRP, POSFine.PRP_dollar]

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        entity = Entity(node.word, node.lemma, node.index, entity_type=default_type, explicit=explicit_entities)
        new = [entity]
        if node.arc in [Arc.nsubj] and node.pos_fine in [POSFine.PRP, POSFine.PRP_dollar]:
            entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.primary_actor_proxy)
            new = [entity]
        elif node.arc in [Arc.nsubjpass, Arc.csubjpass, Arc.csubj]:
            if node.pos_fine in [POSFine.PRP, POSFine.PRP_dollar]:
                if node.arc in carryover.references:
                    entity, new = carryover.references[node.arc].duplicate(dup_up=False, dup_down=False)
                else:
                    entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.object_proxy)
                    new = [entity]
            else:
                carryover.update_references(node, entity)
        print("Genericsubject parent , entity")
        print(parent, entity)
        #Ex: As a user, I want the publish button in FABS to deactivate a page.
        # Publish button is an Interface and the subj of deactivate
        if(carryover.check_and_consume(Flag.interface) and node.pos_fine not in [POSFine.PRP, POSFine.PRP_dollar]):
            entity.entity_type = EntityType.interface
        updated, new_ents = stack(parent, entity)
        return updated, new + new_ents, carryover, node.modifiers


class GenericModifier(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc in Arc.all_mod()

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        if parent:
            if node.arc == Arc.advmod:
                parent.add_pretext(node.word)
            else:
                parent.add_description(node.word)
        return parent, [], carryover, node.modifiers


class PassiveAuxiliary(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        # return node.arc == Arc.auxpass
        return False

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        parent.add_pretext(node.word)
        return parent, [], carryover, node.modifiers


class PhrasalVerbParticle(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.prt

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class Preposition(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.prep and any(GenericObject.check_conditions(n) for n in node.modifiers)

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        
        entity = Entity(node.word, node.lemma, index=node.index, entity_type=EntityType.extension)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class Negation(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.neg

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        # if a word like "not" add it like you would any other word. If contracted, don't or haven't, add to pretext
        if "'" in node.word:
            if parent:
                parent.add_pretext(node.word, merge=True)
            return parent, [], carryover, node.modifiers
        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class RelativeClausalModifier(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.relcl

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        
        if parent :

            ancestor_relation = parent
            while(ancestor_relation and ancestor_relation.entity_type != EntityType.relation):
                ancestor_relation = ancestor_relation.parent
           
            if ancestor_relation:
                if any([mod for mod in node._modifiers if mod.word.lower() in ["i","we"]]):
                    # Exple : As a Data Publishing User, I want to be able to delete a dataset I have published
                    # this information is too vague (I have published) to be modelled
                    return parent, [] , carryover, []

                
            
            if(node.pos_fine == POSFine.VB and (any([mod for mod in node._modifiers if mod.word.lower() == "to"]))):
                entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.relation)
                
                if(ancestor_relation):
                    #Exple:"As a user, I want to have a better place to collect project materials"

                    updated, new_ents = stack(ancestor_relation, entity)
                else:
                    #Ex: As a Developer , I want the historical FPDS data loader to include both extracted historical data and FPDS feed data.

                    parent.entity_type = EntityType.interface
                    updated, new_ents = stack(parent, entity)
                return updated, [entity] + new_ents, carryover, node.modifiers

        # Exple :"I want to have a website that is easy to use"  "is" == relcl
        # Quality attribute in this type of sent

        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers

            


class Dative(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.dative

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:

        entity = Entity(node.word, node.lemma, node.index, entity_type=EntityType.extension)
        updated, new_ents = stack(parent, entity)
        return updated, [entity] + new_ents, carryover, node.modifiers


class Comma(ParseRule):

    @staticmethod
    def check_conditions(node: Node):
        return node.arc == Arc.punct and node.pos_fine == POSFine.COMMA

    @staticmethod
    def parse(node: Node, parent: Entity, default_type: EntityType, explicit_entities: bool, carryover: Carryover) \
            -> Tuple[Entity, List[Entity], Carryover, List[Node]]:
        for sibling in node.parent.modifiers:
            if Conjunct.check_conditions(sibling):
                for mod in sibling.modifiers:
                    if Comma.check_conditions(mod):
                        return Comma.parse(mod, parent, default_type, explicit_entities, carryover)
                    elif Conjunction.check_conditions(mod):
                        return Conjunction.parse(mod, parent, default_type, explicit_entities, carryover)
        return parent, [], carryover, node.modifiers





rules: Set[ParseRule] = frozenset([ClausalComplement, OpenClausalComplement, AdverbialClausalModifier, ClausalModifier,
                                   PossessiveNominalModifier, PassiveAuxiliary, Dative, Case, Comma, Attribute,
                                   PhrasalVerbParticle, Preposition, AdjectivalComplement, Negation, GenericContext,
                                   RelativeClausalModifier, GenericObject, GenericModifier, Conjunction, Conjunct,
                                   GenericSubject, Compound, PrepositionComplement])


__all__ = ["rules", "ParseRule", "Carryover", "Flag"]

###TODO REMOVE Only for test ###
import pandas as pd
import os
def add_df_analyse(name,us,word,output):
    print("PATH : ",os.listdir())
    if("djangoserver"in os.listdir()):
        path = "./djangoserver/out/test_rule/"+name+".csv"
    else:
        path = "./out/test_rule/"+name+".csv"
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        df = pd.DataFrame(columns = ['us' , 'word', 'output' ])
    df = df.append({'us' : us , 'word' : word,'output':output} , ignore_index=True)

    df.to_csv(path,index=False)