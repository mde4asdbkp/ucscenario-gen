import spacy
import subprocess
import os
from importlib.util import find_spec
from datetime import datetime
from typing import List
import time
import traceback
import pandas as pd

use_src = find_spec("src")
if use_src:
    from src.storyparser.tree_parser import TreeParser
    from src.uml.diagram import Diagram
    from src.util import Logger
    from src.storyparser.entity import Entity, EntityType, Property
    from src.preprocessing.preprocessing import Preprocessing


else:
    import sys
    sys.path.insert(0, "../")
    from storyparser.tree_parser import TreeParser
    from uml.diagram import Diagram
    from util import Logger
    from storyparser.entity import Entity, EntityType, Property
    from preprocessing.preprocessing import Preprocessing




class DiagramGenerator:
    #TODO use en_core_web_md in prod
    nlp = spacy.load("en_core_web_md")
    #nlp = spacy.load("en")

    plant_uml = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "plant_uml", "plantuml.jar"))

    @staticmethod
    def parse(story: str, logger: Logger, parser: TreeParser=None):
        if not parser:
            parser = TreeParser()
        doc_spacy = DiagramGenerator.nlp(story)
        parser.entities(doc_spacy, logger,DiagramGenerator.nlp)
        
        return parser

    @staticmethod
    def make(parser, logger, id, stories: List[str]=None, display_all: bool=False, type_dia="single"):
        """
        Creation of a diagram based on the entities in the parser. 
        plant_uml generates a PNG to model this diagram in ./out/'type_dia'
        Returns the plant_uml code
        """
        diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)
        diagram_out = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out", type_dia, id + ".txt"))
        diagram_file = open(diagram_out, "w+")
        diagram_file.write(diagram.textual_representation)
        diagram_file.close()
        time.sleep(0.2)

        print(DiagramGenerator.plant_uml)
        print(diagram_out)
        print(subprocess.check_output(["bash", "-c", "java -jar '" + DiagramGenerator.plant_uml + "' '" + diagram_out + "'"]))
        print(" on a créé le fichier : ",diagram_out)
        print("INFO NB_ ", diagram.count_do())

        return diagram.textual_representation
        

    @staticmethod
    def print_log(logger, story_id,type_dia="single"):
        logger.print_to_file(
            os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out",type_dia)),
            story_id + "_LOG.txt"
        )

    @staticmethod
    def generate(story: str):
        """
        Generation of the diagram for the US story in ./out/single/
        Returns the US id (png name) and the plant_uml code used to generate the PNG
        """
        story_id = str(datetime.now().timestamp())
        parser = TreeParser()

        logger = Logger(story_id)
        story = story.replace("\n", "")
        story_preprocess = Preprocessing().process([story])
        story_split,_ = story_preprocess[0]
        for s in story_split:
            p = DiagramGenerator.parse(s, logger, parser=parser)
        result = {}
        result["id"] = story_id
        result["plant_uml"] = DiagramGenerator.make(p, logger, story_id, stories=[story],display_all=True)
        DiagramGenerator.print_log(logger, story_id,type_dia="single")
        return result  
    
    
    @staticmethod
    def multi(stories: List[str], display_all: bool):
        """
        Generation of the diagram for the US story in ./out/single/
        Returns the US id (png name) and the plant_uml code used to generate the PNG
        """

        print("DIAGRAM_TYPE", display_all)
        print("MULTI_US")
        parser = TreeParser()
        story_id = str(datetime.now().timestamp())
        logger = Logger(story_id)
        stories_split = []
        
        stories = Preprocessing().clean_symbol(stories)
        notes = []
        stories = [s for s in stories if s != ""]
        for story in stories:
            story_preprocess = Preprocessing().process([story])
            story_split,_ = story_preprocess[0]

            for s in story_split:
                notes.append(story)
                p = DiagramGenerator.parse(s, logger, parser=parser)
        result = {}
        result["id"] = story_id
        result["plant_uml"] = DiagramGenerator.make(p, logger, story_id, stories=notes, display_all=True,type_dia="multi")
        DiagramGenerator.print_log(logger, story_id,type_dia="multi")
        
        return result

    
    @staticmethod
    def multi_view_by_object(stories: List[str],type_dia):
        """
        Generation of diagrams by view (a main object) based on the US in stories.
        We can generate views for DOs of type Actor, Boundary and Entity
        They will be saved in ./out/'type_dia' /'main_id' /
        Returns the main_id and the list of info by diagram
        """
        print("VIEW_DIAGRAM_TYPE")
        parser = TreeParser()
        story_id = str(datetime.now().timestamp())
        logger = Logger(story_id)
        stories_split = []
        
        stories = Preprocessing().clean_symbol(stories)
        notes = []
        for story in stories:
            story_preprocess = Preprocessing().process([story])
            story_split,_ = story_preprocess[0]

            for s in story_split:
                notes.append(story)
                p = DiagramGenerator.parse(s, logger, parser=parser)
        result = {}
        result["main_id"] = story_id
        
        result["list_view"] = DiagramGenerator.make_diagram_by_object(p, logger, story_id, stories=notes, display_all=True,type_dia=type_dia)
        DiagramGenerator.print_log(logger, story_id,type_dia=type_dia)
        return result

    def make_diagram_by_object(parser, logger, id, stories: List[str]=None, display_all: bool=False,type_dia="view_actor"):
        """
        Generation of diagrams by view (a main object) based on the US in stories.
        We can generate views for DOs of type Actor, Boundary and Entity
        They will be saved in ./out/'type_dia' /'main_id' /
        Returns  the list of info by diagram
        """
        diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=True)
        path_save = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out"))
        if(type_dia not in os.listdir(path_save)):
            os.mkdir(path_save+"/"+type_dia)
        path_save = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out",type_dia))

        list_do=[]
        if(type_dia == "view_actor"):
            list_do = diagram.all_actor
        elif(type_dia == "view_boundary"):
            list_do = diagram.all_boundary
        elif(type_dia == "view_entity"):
            list_do = diagram.all_entity
        
        list_view = []

        i=0
        for do in list_do:
            textual, list_us = diagram.textual_representation_by_view(do)
            if textual:
                try:
                    i+=1
                    dir_name = str(id)
                    if(dir_name not in os.listdir(path_save)):
                        print("PATH__:",path_save+dir_name)
                        os.mkdir(path_save+"/"+dir_name)

                    diagram_out = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out", type_dia,dir_name,str(i) + ".txt"))
                    print("diagram_out :",diagram_out)
                    diagram_file = open(diagram_out, "w+")
                    diagram_file.write(textual)
                    diagram_file.close()
                    
                    DiagramGenerator.save_US(list_us,diagram_out)

                    print("Finish "+str(i))
                    print(subprocess.check_output(["bash", "-c", "java -jar '" + DiagramGenerator.plant_uml + "' '" + diagram_out + "'"]))
                    list_view.append({"name":do.pretty_name.replace("\n", " "),"id":i,"plant_uml":textual,"list_us":list_us})
                except Exception as e:
                    print(e)
                    logger.log("error", traceback.format_exc())
                
        return list_view

    def multi_split(stories: List[str]):
        """
        Generation of sub-diagrams composing the generated diagram for US stories.
        They will be saved in./out/multi_split/id_main/
        Returns the main_id and the list of info by diagram
        """

        print("DIAGRAM_SPLIT")
        print("MULTI_US")
        parser = TreeParser()
        story_id = str(datetime.now().timestamp())
        logger = Logger(story_id)
        stories_split = []
        
        stories = Preprocessing().clean_symbol(stories)
        notes = []
        for story in stories:
            story_preprocess = Preprocessing().process([story])
            story_split,_ = story_preprocess[0]

            for s in story_split:
                notes.append(story)
                p = DiagramGenerator.parse(s, logger, parser=parser)
        result={}
        result["main_id"] = story_id
        
        result["list_view"] = DiagramGenerator.make_sub_diagram(p, logger, story_id, stories=notes, display_all=True)
        DiagramGenerator.print_log(logger, story_id,type_dia="multi_split")
        return result


    @staticmethod
    def make_sub_diagram(parser, logger, id, stories: List[str]=None, display_all: bool=False):

        """
        Generation of sub-diagrams composing the generated diagram for US stories.
        They will be saved in./out/multi_split/id_main/
        Returns  the list of info by diagram generate
        """
        path_save = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out","multi_split"))
        diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)
        if(str(id) not in os.listdir(path_save)):
            os.mkdir(path_save+"/"+str(id))
        path_save = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out","multi_split",str(id)))
        list_view = []
        print("path_save : ",path_save)

        for i in range (0,len(diagram.sub_textual_representation)):
            try:
                
                diagram_out = os.path.abspath(path_save+"/"+str(i)+".txt")
                print(diagram_out)
                diagram_file = open(diagram_out, "w+")
                textual_representation, list_us = diagram.sub_textual_representation[i]
                diagram_file.write(textual_representation)
                diagram_file.close()
                print("Finish "+str(i))
                print(subprocess.check_output(["bash", "-c", "java -jar '" + DiagramGenerator.plant_uml + "' '" + diagram_out + "'"]))
                
                DiagramGenerator.save_US(list_us,diagram_out)
                
                list_view.append({"name":i,"id":i,"plant_uml":textual_representation,"list_us":list_us})
            except Exception as e:
                print("on a un problème au MAKE")
                print("error", traceback.format_exc())
                logger.log("error", traceback.format_exc())
        
        return list_view

    @staticmethod
    def save_US(list_us, path_txt):
        df_us_output = pd.DataFrame()
        df_us_output["US"] = list_us
        df_us_output.to_csv(path_txt.replace(".txt",".csv"))
 