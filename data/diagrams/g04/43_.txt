
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Questions" as thinguserquestions #grey
	actor "Superuser" as actorsuperuser
	boundary "Questions\nInterface" as thingquestionsinterface #grey
	control "Reply To\nUser Questions" as controlreplyuserquestions

	actorsuperuser --- thingquestionsinterface
	thinguserquestions --- controlreplyuserquestions
	thingquestionsinterface --> controlreplyuserquestions

@enduml