
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Status" as thingstatus
	entity "Site" as thingsite
	entity "Dashboard" as thingdashboard
	actor "Admin" as actoradmin
	boundary "Status\nInterface" as thingstatusinterface #grey
	control "View A\nDashboard Monitors\nThe Sites'\nStatuses" as controlviewdashboard

	thingsite <.. thingstatus
	thingstatus <.. thingdashboard
	actoradmin --- thingstatusinterface
	thingdashboard --- controlviewdashboard
	thingstatus --- controlviewdashboard
	thingstatusinterface --> controlviewdashboard

@enduml