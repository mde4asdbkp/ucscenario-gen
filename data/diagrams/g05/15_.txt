
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Visualisation" as thingvisualisation
	entity "Datum" as thingdatum
	actor "Data ,\nConsume User" as actordataconsuminguser
	circle "Csv" as thingcsv
	boundary "Visualisation\nInterface" as thingvisualisationinterface #grey
	control "Viewing" as controlviewdownloadcsv
	control "Download A\nCsv Of\nThe Data\nUsed In\nAny Visualisation" as controldownloadcsv

	thingvisualisation <.. thingdatum
	thingdatum *-- thingcsv
	actordataconsuminguser --- thingvisualisationinterface
	thingvisualisationinterface --> controlviewdownloadcsv
	controlviewdownloadcsv --> controldownloadcsv
	thingcsv --- controldownloadcsv
	thingdatum --- controldownloadcsv
	thingvisualisation --- controldownloadcsv
	thingvisualisationinterface --> controldownloadcsv

@enduml