
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Place" as thingplace
	entity "Documentation" as thingdocumentation
	entity "Related Report" as thingrelatedreport
	actor "Team" as actorteam
	actor "Nsf Member" as actornsfmember
	boundary "Report\nInterface" as thingreportinterface #grey
	boundary "Documentation\nInterface" as thingdocumentationinterface #grey
	boundary "Place\nInterface" as thingplaceinterface #grey
	control "Access The\nRelated Reports" as controlaccesshaveplacerelatedreport
	control "Access Documentation" as controlaccesshaveplacemoinsPRONmoinsdocumentation
	control "Have A\nPlace" as controlhaveplace

	actornsfmember --- thingreportinterface
	actorteam --- thingreportinterface
	actorteam --- thingplaceinterface
	actornsfmember --- thingplaceinterface
	thingrelatedreport --- controlaccesshaveplacerelatedreport
	thingreportinterface --> controlaccesshaveplacerelatedreport
	thingdocumentation --- controlaccesshaveplacemoinsPRONmoinsdocumentation
	thingdocumentationinterface --> controlaccesshaveplacemoinsPRONmoinsdocumentation
	controlaccesshaveplacerelatedreport --> controlhaveplace
	thingplace --- controlhaveplace
	thingplaceinterface --> controlhaveplace
	controlaccesshaveplacemoinsPRONmoinsdocumentation --> controlhaveplace

@enduml