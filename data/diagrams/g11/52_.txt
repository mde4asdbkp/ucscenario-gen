
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Recruiter" as thingrecruiter
	actor "Web ,\nRecruiter Manager" as actorwebrecruitermanager
	boundary "Recruiter\nInterface" as thingrecruiterinterface #grey
	control "Confirm The\nRecruiter" as controlconfirmrecruiter

	actorwebrecruitermanager --- thingrecruiterinterface
	thingrecruiter --- controlconfirmrecruiter
	thingrecruiterinterface --> controlconfirmrecruiter

@enduml