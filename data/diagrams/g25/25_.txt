
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Probable Importance" as thingprobableimportance
	entity "User" as thinguser
	entity "Object" as thingobject
	entity "Description" as thingdescription
	actor "Repository Manager" as actorrepositorymanager
	circle "External Version" as thingexternalversion
	boundary "User\nInterface" as thinguserinterface #grey
	boundary "Description\nInterface" as thingdescriptioninterface #grey
	control "Indicate Any\nExternal Versions\nOf An\nObject Be\nOf Probable\nImportance To\nAn User" as controlindicateexternalversion
	control "Indicate Description" as controlindicatedescription

	thinguser <.. thingprobableimportance
	thingprobableimportance <.. thingobject
	thingobject *-- thingexternalversion
	actorrepositorymanager --- thinguserinterface
	actorrepositorymanager --- thingdescriptioninterface
	thingexternalversion --- controlindicateexternalversion
	thingobject --- controlindicateexternalversion
	thingprobableimportance --- controlindicateexternalversion
	thinguser --- controlindicateexternalversion
	thinguserinterface --> controlindicateexternalversion
	thingdescription --- controlindicatedescription
	thingdescriptioninterface --> controlindicatedescription

@enduml