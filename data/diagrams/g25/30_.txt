
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	actor "User" as actoruser
	boundary "Object" as thingobjectuser
	control "What Belongs\nTo" as controlbelongobjectuserwhat
	control "Know" as controlknowbelongobjectuserwhat

	actoruser --- thingobjectuser
	thingobjectuser --> controlbelongobjectuserwhat
	controlknowbelongobjectuserwhat --> controlbelongobjectuserwhat
	thingobjectuser --> controlknowbelongobjectuserwhat

@enduml