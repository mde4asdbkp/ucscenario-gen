
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Access" as thingaccess
	entity "Object" as thingobject
	entity "Content Files" as thingcontentfiles #grey
	actor "User" as actoruser

	thingcontentfiles <.. thingaccess
	thingobject <.. thingcontentfiles

@enduml