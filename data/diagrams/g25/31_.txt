
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	actor "User" as actoruser
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "See The\nComponent Parts\nOf An\nObject" as controlseecomponentpart

	actoruser --- thingobjectinterface
	thingobject --- controlseecomponentpart
	thingobjectinterface --> controlseecomponentpart

@enduml