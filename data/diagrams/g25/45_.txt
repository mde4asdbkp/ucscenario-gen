
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Life Cycle" as thinglifecycle #grey
	entity "Component" as thingcomponent
	entity "Event" as thingevent
	actor "Repository Manager" as actorrepositorymanager
	boundary "Cycle\nInterface" as thingcycleinterface #grey
	control "Know All\nEvents To\nA Component\nOver Life\nCycle" as controlknowevent

	thinglifecycle <.. thingcomponent
	thingcomponent <.. thingevent
	actorrepositorymanager --- thingcycleinterface
	thingevent --- controlknowevent
	thingcomponent --- controlknowevent
	thinglifecycle --- controlknowevent
	thingcycleinterface --> controlknowevent

@enduml