
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	actor "Archive ,\nData ,\nBath Administrator" as actorarchivedatabathadministrator
	circle "Schedule Disposal" as thingscheduleddisposal
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Approve Scheduled\nDisposal Of\nData" as controlapprovescheduleddisposal

	thingdatum *-- thingscheduleddisposal
	actorarchivedatabathadministrator --- thingdatuminterface
	thingscheduleddisposal --- controlapprovescheduleddisposal
	thingdatum --- controlapprovescheduleddisposal
	thingdatuminterface --> controlapprovescheduleddisposal

@enduml