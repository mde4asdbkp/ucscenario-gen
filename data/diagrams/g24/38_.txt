
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Bath Data" as thingbathdata #grey
	entity "Data Centre\nWholesale" as thingdatacentrewholesale
	actor "Archive ,\nData ,\nBath Administrator" as actorarchivedatabathadministrator
	boundary "Wholesale\nInterface" as thingwholesaleinterface #grey
	control "Import Bath\nData From\nAn External\nData Centre\nWholesale" as controlimportbathdataexternaldatacentrewholesale

	thingdatacentrewholesale <.. thingbathdata
	actorarchivedatabathadministrator --- thingwholesaleinterface
	thingbathdata --- controlimportbathdataexternaldatacentrewholesale
	thingdatacentrewholesale --- controlimportbathdataexternaldatacentrewholesale
	thingwholesaleinterface --> controlimportbathdataexternaldatacentrewholesale

@enduml