
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Tag Staff" as thingtagstaff #grey
	entity "Working Papers" as thingworkingpapers #grey
	actor "Assistant Archivist" as actorassistantarchivist
	boundary "Papers\nInterface" as thingpapersinterface #grey
	control "Generated Upload\nTag Staff\nWorking Papers" as controluploadtagstaffworkingpapers

	thingworkingpapers <.. thingtagstaff
	actorassistantarchivist --- thingpapersinterface
	thingtagstaff --- controluploadtagstaffworkingpapers
	thingworkingpapers --- controluploadtagstaffworkingpapers
	thingpapersinterface --> controluploadtagstaffworkingpapers

@enduml