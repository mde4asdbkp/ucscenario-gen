
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Online Form" as thingonlineform
	entity "Archival Material" as thingarchivalmaterial #grey
	actor "Archivist" as actorarchivist
	boundary "Material\nInterface" as thingmaterialinterface #grey
	control "Have An\nOnline Form\nLicensing Archival\nMaterial" as controlhaveonlineform

	thingarchivalmaterial <.. thingonlineform
	actorarchivist --- thingmaterialinterface
	thingonlineform --- controlhaveonlineform
	thingarchivalmaterial --- controlhaveonlineform
	thingmaterialinterface --> controlhaveonlineform

@enduml