
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Unprocessed Material" as thingunprocessedmaterial
	entity "Access" as thingaccess
	actor "Archivist" as actorarchivist
	circle "Digital Copy" as thingdigitalcopy
	boundary "Material\nInterface" as thingmaterialinterface #grey
	control "Provide Access\nTo Digital\nCopies Of\nUnprocessed Material" as controlprovideaccess

	thingdigitalcopy <.. thingaccess
	thingunprocessedmaterial *-- thingdigitalcopy
	actorarchivist --- thingmaterialinterface
	thingaccess --- controlprovideaccess
	thingdigitalcopy --- controlprovideaccess
	thingunprocessedmaterial --- controlprovideaccess
	thingmaterialinterface --> controlprovideaccess

@enduml