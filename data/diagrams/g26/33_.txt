
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Citation Data" as thingcitationdata
	actor "Researcher" as actorresearcher
	boundary "Data\nInterface" as thingdatainterface #grey
	control "Generate Structured\nCitation Data" as controlgeneratestructuredcitationdata

	actorresearcher --- thingdatainterface
	thingcitationdata --- controlgeneratestructuredcitationdata
	thingdatainterface --> controlgeneratestructuredcitationdata

@enduml