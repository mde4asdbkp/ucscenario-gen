
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Form" as thingform
	entity "Preservation Photos" as thingpreservationphotos #grey
	entity "Documentation" as thingdocumentation #grey
	entity "Correspondence" as thingcorrespondence
	entity "Loan" as thingloan #grey
	actor "Archivist" as actorarchivist
	boundary "Form\nInterface" as thingforminterface #grey
	boundary "Photos\nInterface" as thingphotosinterface #grey
	boundary "Correspondence\nInterface" as thingcorrespondenceinterface #grey
	control "Manage Documentation\nIncluding Form" as controlmanagedocumentationform
	control "Manage Loan\nDocumentation Including\nPreservation Photos" as controlmanageloandocumentation
	control "Manage Documentation\nIncluding Correspondence" as controlmanagedocumentationcorrespondence

	thingform <.. thingdocumentation
	thingcorrespondence <.. thingdocumentation
	thingloan <.. thingdocumentation
	thingpreservationphotos <.. thingdocumentation
	actorarchivist --- thingforminterface
	actorarchivist --- thingphotosinterface
	actorarchivist --- thingcorrespondenceinterface
	thingdocumentation --- controlmanagedocumentationform
	thingform --- controlmanagedocumentationform
	thingforminterface --> controlmanagedocumentationform
	thingpreservationphotos --- controlmanageloandocumentation
	thingphotosinterface --> controlmanageloandocumentation
	thingdocumentation --- controlmanageloandocumentation
	thingcorrespondence --- controlmanagedocumentationcorrespondence
	thingcorrespondenceinterface --> controlmanagedocumentationcorrespondence
	thingdocumentation --- controlmanagedocumentationcorrespondence

@enduml