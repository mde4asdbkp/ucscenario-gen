
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Keyword" as thingkeyword
	entity "File" as thingfile
	actor "Archivist" as actorarchivist
	boundary "Keyword\nInterface" as thingkeywordinterface #grey
	control "Search All\nFiles By\nKeyword" as controlsearchfilekeyword

	thingkeyword <.. thingfile
	actorarchivist --- thingkeywordinterface
	thingfile --- controlsearchfilekeyword
	thingkeyword --- controlsearchfilekeyword
	thingkeywordinterface --> controlsearchfilekeyword

@enduml