
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Photographer" as thingphotographer
	entity "Image" as thingimage
	actor "Archivist" as actorarchivist
	boundary "Photographer\nInterface" as thingphotographerinterface #grey
	control "Search Images\nBy Photographer" as controlsearchimagephotographer

	thingphotographer <.. thingimage
	actorarchivist --- thingphotographerinterface
	thingimage --- controlsearchimagephotographer
	thingphotographer --- controlsearchimagephotographer
	thingphotographerinterface --> controlsearchimagephotographer

@enduml