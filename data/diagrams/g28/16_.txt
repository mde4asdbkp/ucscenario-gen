
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Geographical Map" as thinggeographicalmap
	actor "User" as actoruser
	boundary "Map\nInterface" as thingmapinterface #grey
	control "Walk" as controlwalk
	control "Use A\nGeographical Map" as controlusegeographicalmap

	actoruser --- thingmapinterface
	thinggeographicalmap --- controlusegeographicalmap
	thingmapinterface --> controlusegeographicalmap

@enduml