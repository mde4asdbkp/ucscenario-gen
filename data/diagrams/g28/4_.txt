
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Image Tag" as thingimagetag #grey
	entity "Music" as thingmusic
	entity "Enrich Metadata" as thingenrichedmetadata
	actor "User" as actoruser
	boundary "Tag\nInterface" as thingtaginterface #grey
	boundary "Metadata\nInterface" as thingmetadatainterface #grey
	control "Search Music\nExploiting Image\nTag" as controlsearchmusicimagetag
	control "Search Music\nExploiting Enriched\nMetadata" as controlsearchmusicenrichedmetadata
	control "Discover Music\nExploiting Image\nTag" as controldiscovermusicimagetag
	control "Discover Music\nExploiting Enriched\nMetadata" as controldiscovermusicenrichedmetadata

	thingenrichedmetadata <.. thingmusic
	thingimagetag <.. thingmusic
	actoruser --- thingtaginterface
	actoruser --- thingmetadatainterface
	thingimagetag --- controlsearchmusicimagetag
	thingtaginterface --> controlsearchmusicimagetag
	thingmusic --- controlsearchmusicimagetag
	thingmusic --- controlsearchmusicenrichedmetadata
	thingenrichedmetadata --- controlsearchmusicenrichedmetadata
	thingmetadatainterface --> controlsearchmusicenrichedmetadata
	thingimagetag --- controldiscovermusicimagetag
	thingtaginterface --> controldiscovermusicimagetag
	thingmusic --- controldiscovermusicimagetag
	thingmusic --- controldiscovermusicenrichedmetadata
	thingenrichedmetadata --- controldiscovermusicenrichedmetadata
	thingmetadatainterface --> controldiscovermusicenrichedmetadata

@enduml