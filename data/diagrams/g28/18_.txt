
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Video" as thingvideo
	actor "Admin" as actoradmin
	circle "Segmentation" as thingsegmentation
	boundary "Video\nInterface" as thingvideointerface #grey
	control "See Segmentation\nOf Videos" as controlseesegmentation
	control "Validate Segmentation\nOf Videos" as controlvalidatesegmentation
	control "Edit Segmentation\nOf Videos" as controleditsegmentation

	thingvideo *-- thingsegmentation
	actoradmin --- thingvideointerface
	thingsegmentation --- controlseesegmentation
	thingvideo --- controlseesegmentation
	thingvideointerface --> controlseesegmentation
	thingvideo --- controlvalidatesegmentation
	thingsegmentation --- controlvalidatesegmentation
	thingvideointerface --> controlvalidatesegmentation
	thingvideo --- controleditsegmentation
	thingsegmentation --- controleditsegmentation
	thingvideointerface --> controleditsegmentation

@enduml