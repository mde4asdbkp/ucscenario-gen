
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Area" as thingarea
	entity "Content Recommendations" as thingcontentrecommendations #grey
	entity "News Events" as thingnewsevents
	actor "User" as actoruser
	boundary "Area\nInterface" as thingareainterface #grey
	control "Receive Content\nRecommendations On\nMost Relevant\nNews Events\nIn Area" as controlreceivecontentrecommendations

	thingnewsevents <.. thingcontentrecommendations
	thingarea <.. thingnewsevents
	actoruser --- thingareainterface
	thingcontentrecommendations --- controlreceivecontentrecommendations
	thingnewsevents --- controlreceivecontentrecommendations
	thingarea --- controlreceivecontentrecommendations
	thingareainterface --> controlreceivecontentrecommendations

@enduml