
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Plan Reviews" as thingplanreviews
	actor "Plan ,\nReview ,\nStaff Member" as actorplanreviewstaffmember
	circle "Completion" as thingcompletion
	boundary "Reviews\nInterface" as thingreviewsinterface #grey
	control "Track The\nCompletion Of\nRequired Plan\nReviews" as controltrackcompletion

	thingplanreviews *-- thingcompletion
	actorplanreviewstaffmember --- thingreviewsinterface
	thingcompletion --- controltrackcompletion
	thingplanreviews --- controltrackcompletion
	thingreviewsinterface --> controltrackcompletion

@enduml