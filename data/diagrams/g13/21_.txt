
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Password" as thingpassword
	entity "Account Name" as thingaccountname
	actor "Moderator" as actormoderator
	boundary "Password\nInterface" as thingpasswordinterface #grey
	boundary "Name\nInterface" as thingnameinterface #grey
	control "Log In\nUsing Password" as controllogpassword
	control "Log In\nUsing Account\nName" as controllogaccountname

	actormoderator --- thingpasswordinterface
	actormoderator --- thingnameinterface
	thingpassword --- controllogpassword
	thingpasswordinterface --> controllogpassword
	thingaccountname --- controllogaccountname
	thingnameinterface --> controllogaccountname

@enduml