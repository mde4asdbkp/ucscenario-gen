
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Estimate" as thingestimate
	entity "-pron-" as thingmoinsPRONmoins #grey
	entity "Timer" as thingtimer #grey
	entity "Minute" as thingminute #grey
	actor "Participant" as actorparticipant
	boundary "-Pron-\nInterface" as thingproninterface #grey
	boundary "Estimate\nInterface" as thingestimateinterface #grey
	control "The Timer\nSoon As\nReset Itself\nAll Play" as controlplayhavetimermoinsPRONmoinsall
	control "Have" as controlhave
	control "The Minute\nTimer Soon\nAs Reset\nPlay An\nEstimate" as controlplayhaveminutetimerestimate

	thingmoinsPRONmoins <.. thingtimer
	thingminute <.. thingtimer
	thingestimate <.. thingtimer
	actorparticipant --- thingproninterface
	actorparticipant --- thingestimateinterface
	thingtimer --- controlplayhavetimermoinsPRONmoinsall
	thingmoinsPRONmoins --- controlplayhavetimermoinsPRONmoinsall
	thingproninterface --> controlplayhavetimermoinsPRONmoinsall
	controlplayhaveminutetimerestimate --> controlhave
	thingestimateinterface --> controlhave
	controlplayhavetimermoinsPRONmoinsall --> controlhave
	thingestimate --- controlplayhaveminutetimerestimate
	thingtimer --- controlplayhaveminutetimerestimate
	thingestimateinterface --> controlplayhaveminutetimerestimate

@enduml