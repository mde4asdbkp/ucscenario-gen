
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Participant" as thingparticipant
	entity "Minute Countdown\nTimer" as thingminutecountdowntimer #grey
	actor "Participant" as actorparticipant
	boundary "Participant\nInterface" as thingparticipantinterface #grey
	control "Start A\nMinute Countdown\nTimer That\nAll Participants\nSee" as controlstartminutecountdowntimer

	thingparticipant <.. thingminutecountdowntimer
	actorparticipant --- thingparticipantinterface
	thingminutecountdowntimer --- controlstartminutecountdowntimer
	thingparticipant --- controlstartminutecountdowntimer
	thingparticipantinterface --> controlstartminutecountdowntimer

@enduml