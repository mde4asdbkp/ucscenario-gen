
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Account Details" as thingaccountdetails #grey
	actor "Moderator" as actormoderator
	boundary "Details\nInterface" as thingdetailsinterface #grey
	control "Change Account\nDetails" as controlchangeaccountdetails

	actormoderator --- thingdetailsinterface
	thingaccountdetails --- controlchangeaccountdetails
	thingdetailsinterface --> controlchangeaccountdetails

@enduml