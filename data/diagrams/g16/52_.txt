
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ezid" as thingEZID
	entity "Assign Doi" as thingassigneddoi
	entity "Item" as thingitem
	actor "Collection Curator" as actorcollectioncurator

	thingassigneddoi <.. thingitem

@enduml