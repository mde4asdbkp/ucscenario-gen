
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Person" as thingperson
	entity "Name Authority\nControl" as thingnameauthoritycontrol #grey
	entity "System" as thingothersystem
	entity "Duid" as thingDUID
	entity "Symplectic Element" as thingsymplecticelement
	entity "Creator" as thingcreator
	entity "Interoperability" as thinginteroperability
	actor "Repository Administrator" as actorrepositoryadministrator
	boundary "Author" as thingauthorrepositoryadministrator
	boundary "Person\nInterface" as thingpersoninterface #grey
	boundary "Creator\nInterface" as thingcreatorinterface #grey
	control "Allow For\nInteroperability With\nOther Systems\nLike Symplectic\nElements" as controlallowinteroperability
	control "Store The\nDuid Of\nThe Person" as controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol
	control "Creator Store" as controlstorecreator
	control "Allow For\nName Authority\nControl" as controlallownameauthoritycontrol

	thingsymplecticelement <.. thingothersystem
	thingperson *-- thingDUID
	thingothersystem <.. thinginteroperability
	actorrepositoryadministrator --- thingauthorrepositoryadministrator
	actorrepositoryadministrator --- thingpersoninterface
	actorrepositoryadministrator --- thingcreatorinterface
	thinginteroperability --- controlallowinteroperability
	thingothersystem --- controlallowinteroperability
	thingsymplecticelement --- controlallowinteroperability
	thingpersoninterface --> controlallowinteroperability
	controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol --> controlallowinteroperability
	thingauthorrepositoryadministrator --> controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol
	thingDUID --- controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol
	thingperson --- controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol
	thingcreator --- controlstorecreator
	thingcreatorinterface --> controlstorecreator
	controlstoreauthorrepositoryadministratorDUIDallownameauthoritycontrol --> controlallownameauthoritycontrol
	thingnameauthoritycontrol --- controlallownameauthoritycontrol
	thingauthorrepositoryadministrator --> controlallownameauthoritycontrol

@enduml