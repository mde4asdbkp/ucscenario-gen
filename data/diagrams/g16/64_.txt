
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Submitter" as thingsubmitter
	entity "Exist Item" as thingexistingitem
	actor "Collection Owner" as actorcollectionowner
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	control "Be" as controlbemodifyexistingitem
	control "Modify Existing\nItems In\nThat Collection" as controlmodifyexistingitem

	thingcollection <.. thingexistingitem
	actorcollectionowner --- thingcollectioninterface
	thingcollectioninterface --> controlbemodifyexistingitem
	controlbemodifyexistingitem --> controlmodifyexistingitem
	thingexistingitem --- controlmodifyexistingitem
	thingcollection --- controlmodifyexistingitem
	thingcollectioninterface --> controlmodifyexistingitem

@enduml