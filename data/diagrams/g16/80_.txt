
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Test" as thingtest
	entity "Version" as thingversion
	entity "Fedora" as thingfedora
	entity "Gem" as thinggem #grey
	entity "Active Fedora" as thingactivefedora
	entity "Ddr Models" as thingddrmodels #grey
	actor "Developer" as actordeveloper
	boundary "Gem\nInterface" as thinggeminterface #grey
	boundary "Fedora\nInterface" as thingfedorainterface #grey
	control "Pass All\nTests On\nFedora" as controlpasstestfedora
	control "Provided With\nA Version\nOf The\nGem" as controlprovideversionpasstestfedora
	control "Provided With\nA Version\nOf The\nDdr Models\nGem Uses\nActive Fedora" as controlprovideversion

	thingfedora <.. thingtest
	thinggem *-- thingversion
	thingddrmodels <.. thinggem
	thingactivefedora <.. thinggem
	actordeveloper --- thinggeminterface
	actordeveloper --- thingfedorainterface
	thingtest --- controlpasstestfedora
	thingfedora --- controlpasstestfedora
	controlprovideversionpasstestfedora --> controlpasstestfedora
	thinggeminterface --> controlpasstestfedora
	thinggem --- controlprovideversionpasstestfedora
	thinggeminterface --> controlprovideversionpasstestfedora
	thingversion --- controlprovideversionpasstestfedora
	thingversion --- controlprovideversion
	thingactivefedora --- controlprovideversion
	thingfedorainterface --> controlprovideversion
	thinggem --- controlprovideversion

@enduml