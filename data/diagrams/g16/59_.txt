
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Format" as thingformat
	entity "Thing" as thingthing
	actor "Potential Depositor" as actorpotentialdepositor
	circle "Kind" as thingkind
	boundary "Format\nInterface" as thingformatinterface #grey
	control "Recommended, Accepted,\nRepository See\nWhat Kinds\nOf Things\nWhat Formats" as controlseekindformat

	thingformat <.. thingthing
	thingthing *-- thingkind
	actorpotentialdepositor --- thingformatinterface
	thingkind --- controlseekindformat
	thingthing --- controlseekindformat
	thingformat --- controlseekindformat
	thingformatinterface --> controlseekindformat

@enduml