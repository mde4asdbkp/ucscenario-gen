
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Descriptive Record" as thingdescriptiverecord
	entity "Fedora" as thingfedora
	entity "Etd Item" as thingetditem #grey
	entity "Use External" as thinguseexternal
	actor "Digital ,\nRecords Archivist" as actordigitalrecordsarchivist
	boundary "Fedora\nInterface" as thingfedorainterface #grey
	control "Expose The\nDescriptive Record\nFor Each\nEtd Item\nFor Use\nExternal To\nFedora" as controlexposedescriptiverecorduseexternal

	thingetditem <.. thingdescriptiverecord
	thinguseexternal <.. thingetditem
	thingfedora <.. thinguseexternal
	actordigitalrecordsarchivist --- thingfedorainterface
	thingdescriptiverecord --- controlexposedescriptiverecorduseexternal
	thingetditem --- controlexposedescriptiverecorduseexternal
	thinguseexternal --- controlexposedescriptiverecorduseexternal
	thingfedora --- controlexposedescriptiverecorduseexternal
	thingfedorainterface --> controlexposedescriptiverecorduseexternal

@enduml