
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ddr" as thingDDR
	actor "Repository Administrator" as actorrepositoryadministrator
	boundary "Symplectic Element" as thingsymplecticelementrepositoryadministrator
	control "Integrate With\nDdr" as controlintegratesymplecticelementrepositoryadministratorDDR

	actorrepositoryadministrator --- thingsymplecticelementrepositoryadministrator
	thingsymplecticelementrepositoryadministrator --> controlintegratesymplecticelementrepositoryadministratorDDR
	thingDDR --- controlintegratesymplecticelementrepositoryadministratorDDR

@enduml