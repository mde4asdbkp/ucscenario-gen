
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Component Level\nMetadata" as thingcomponentlevelmetadata #grey
	actor "Metadata Architect" as actormetadataarchitect
	boundary "Metadata Export" as thingmetadataexportmetadataarchitect
	control "Contain Component\nLevel Metadata" as controlcontainmetadataexportmetadataarchitectcomponentlevelmetadata

	actormetadataarchitect --- thingmetadataexportmetadataarchitect
	thingmetadataexportmetadataarchitect --> controlcontainmetadataexportmetadataarchitectcomponentlevelmetadata
	thingcomponentlevelmetadata --- controlcontainmetadataexportmetadataarchitectcomponentlevelmetadata

@enduml