
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Embargo" as thingembargo
	actor "Digital ,\nRecords Archivist" as actordigitalrecordsarchivist
	boundary "Embargo\nInterface" as thingembargointerface #grey
	control "Remove Embargoes" as controlremoveembargo
	control "Instate Embargoes" as controlinstateembargo

	actordigitalrecordsarchivist --- thingembargointerface
	thingembargo --- controlremoveembargo
	thingembargointerface --> controlremoveembargo
	thingembargo --- controlinstateembargo
	thingembargointerface --> controlinstateembargo

@enduml