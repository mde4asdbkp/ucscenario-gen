
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Group" as thinggroup
	entity "Neurohub Nodes" as thingneurohubnodes #grey
	entity "Mobile Device" as thingmobiledevice
	entity "Share Calendar" as thingsharedcalendar
	entity "Web Page" as thingwebpage #grey
	actor "User" as actoruser
	boundary "Device\nInterface" as thingdeviceinterface #grey
	boundary "Page\nInterface" as thingpageinterface #grey
	control "View The\nGroup Shared\nCalendars On\nMobile Device" as controlviewsharedcalendaron
	control "View The\nGroup's Shared\nCalendars As\nWell Via\nThe Neurohub\nNodes' Web\nPage" as controlviewsharedcalendarwebpage

	thinggroup <.. thingsharedcalendar
	thingwebpage <.. thingsharedcalendar
	thingmobiledevice <.. thingsharedcalendar
	thingneurohubnodes <.. thingwebpage
	actoruser --- thingdeviceinterface
	actoruser --- thingpageinterface
	thingmobiledevice --- controlviewsharedcalendaron
	thingdeviceinterface --> controlviewsharedcalendaron
	thingsharedcalendar --- controlviewsharedcalendaron
	thingsharedcalendar --- controlviewsharedcalendarwebpage
	thingwebpage --- controlviewsharedcalendarwebpage
	thingpageinterface --> controlviewsharedcalendarwebpage

@enduml