
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Neurohub" as thingneurohub
	entity "Script" as thingscript
	entity "Operating System" as thingoperatingsystem
	actor "System Administrator" as actorsystemadministrator
	boundary "Neurohub\nInterface" as thingneurohubinterface #grey
	control "Run A\nScript Installs\nThe Neurohub" as controlrunscript

	thingneurohub <.. thingscript
	actorsystemadministrator --- thingneurohubinterface
	thingscript --- controlrunscript
	thingneurohub --- controlrunscript
	thingneurohubinterface --> controlrunscript

@enduml