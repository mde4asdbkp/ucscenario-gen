
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ability" as thingability
	entity "File" as thingfile
	entity "Minimal Effort" as thingminimaleffort
	entity "Upload Directories" as thinguploaddirectories
	actor "Researcher" as actorresearcher
	boundary "Directories\nInterface" as thingdirectoriesinterface #grey
	boundary "Effort\nInterface" as thingeffortinterface #grey
	control "Have The\nAbility To\nBulk Upload\nDirectories" as controlhaveability
	control "Have The\nAbility To\nFile With\nMinimal Effort" as controlhaveabilityfile

	thinguploaddirectories <.. thingability
	thingfile <.. thingability
	thingminimaleffort <.. thingfile
	actorresearcher --- thingdirectoriesinterface
	actorresearcher --- thingeffortinterface
	thingability --- controlhaveability
	thinguploaddirectories --- controlhaveability
	thingdirectoriesinterface --> controlhaveability
	thingfile --- controlhaveabilityfile
	thingminimaleffort --- controlhaveabilityfile
	thingeffortinterface --> controlhaveabilityfile
	thingability --- controlhaveabilityfile

@enduml