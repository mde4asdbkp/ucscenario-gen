
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Book Log\nPage" as thingbooklogpage #grey
	entity "File" as thingfile
	actor "User" as actoruser
	boundary "File\nInterface" as thingfileinterface #grey
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Search For\nRather Files" as controlsearchfile
	control "Search For\nBook Log\nPage" as controlsearchbooklogpage

	actoruser --- thingfileinterface
	actoruser --- thingpageinterface
	thingfile --- controlsearchfile
	thingfileinterface --> controlsearchfile
	thingbooklogpage --- controlsearchbooklogpage
	thingpageinterface --> controlsearchbooklogpage

@enduml