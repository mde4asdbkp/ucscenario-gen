
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Experiment" as thingexperiment
	actor "User" as actoruser
	boundary "Experimental Run" as thingexperimentalrunbuildexperimentuser #grey
	control "Link" as controllinkexperimentalrunbuildexperimentuser
	control "Build An\nExperiment" as controlbuildexperiment

	actoruser --- thingexperimentalrunbuildexperimentuser
	thingexperimentalrunbuildexperimentuser --> controllinkexperimentalrunbuildexperimentuser
	thingexperimentalrunbuildexperimentuser --> controlbuildexperiment
	thingexperiment --- controlbuildexperiment
	controllinkexperimentalrunbuildexperimentuser --> controlbuildexperiment

@enduml