
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Package" as thingdatapackage #grey
	entity "Clojure" as thingclojure
	entity "Second" as thingsecond
	actor "Developer" as actordeveloper
	boundary "Second\nInterface" as thingsecondinterface #grey
	control "Get A\nData Package\nInto Clojure\nIn Seconds" as controlgetdatapackageclojuresecond

	thingclojure <.. thingdatapackage
	thingsecond <.. thingclojure
	actordeveloper --- thingsecondinterface
	thingdatapackage --- controlgetdatapackageclojuresecond
	thingclojure --- controlgetdatapackageclojuresecond
	thingsecond --- controlgetdatapackageclojuresecond
	thingsecondinterface --> controlgetdatapackageclojuresecond

@enduml