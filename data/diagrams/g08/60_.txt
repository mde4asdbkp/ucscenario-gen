
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Proxy Package" as thingproxypackage #grey
	entity "Reliable" as thingreliable
	entity "App" as thingapp
	entity "Know ,\nData Source" as thingknowdatasource
	entity "Data Packages" as thingdatapackages
	actor "Researcher" as actorresearcher
	boundary "Reliable\nInterface" as thingreliableinterface #grey
	boundary "Source\nInterface" as thingsourceinterface #grey
	control "Provided With\nAn App\nCreate Proxy\nPackages For\nReliable" as controlprovideappreliable
	control "Provided With\nAn App\nCreate Proxy\nData Packages\nFor Well\nKnow, Data\nSources" as controlprovideapp

	thingreliable <.. thingproxypackage
	thingdatapackages <.. thingapp
	thingproxypackage <.. thingapp
	thingknowdatasource <.. thingdatapackages
	actorresearcher --- thingreliableinterface
	actorresearcher --- thingsourceinterface
	thingproxypackage --- controlprovideappreliable
	thingreliable --- controlprovideappreliable
	thingreliableinterface --> controlprovideappreliable
	thingapp --- controlprovideappreliable
	thingapp --- controlprovideapp
	thingdatapackages --- controlprovideapp
	thingknowdatasource --- controlprovideapp
	thingsourceinterface --> controlprovideapp

@enduml