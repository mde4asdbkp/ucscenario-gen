
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ml Datasets" as thingmldatasets #grey
	actor "Machine ,\nLearning Expert" as actormachinelearningexpert
	boundary "Data Packages" as thingdatapackages #grey
	control "Package Ml\nDatasets As\nData Packages" as controlpackagemldatasetsdatapackages

	actormachinelearningexpert --- thingdatapackages
	thingmldatasets --- controlpackagemldatasetsdatapackages
	thingdatapackages --> controlpackagemldatasetsdatapackages

@enduml