
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset Type" as thingdatasettype #grey
	entity "App" as thingapp
	actor "App Developer" as actorappdeveloper
	circle "New Version" as thingnewversion
	circle "Old Version" as thingolderversion
	circle "Code" as thingcode #grey
	boundary "Type\nInterface" as thingtypeinterface #grey
	control "Deploy A\nNew Version\nOf An\nApp Includes\nAn Older\nVersion Of\nA Dataset\nType Another\nApp" as controldeployensurethatnewversion
	control "Expect" as controlexpect #grey
	control "Ensure That" as controlensurethat

	thingapp <.. thingdatasettype
	thingolderversion <.. thingapp
	thingapp *-- thingnewversion
	thingdatasettype *-- thingolderversion
	thingdatasettype *-- thingcode
	actorappdeveloper --- thingtypeinterface
	thingnewversion --- controldeployensurethatnewversion
	thingolderversion --- controldeployensurethatnewversion
	thingdatasettype --- controldeployensurethatnewversion
	thingtypeinterface --> controldeployensurethatnewversion
	thingapp --- controldeployensurethatnewversion
	controldeployensurethatnewversion --> controlensurethat
	thingtypeinterface --> controlensurethat

@enduml