
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Code" as thingcode
	entity "Plugin Artifact" as thingpluginartifact #grey
	entity "Dataset Type" as thingdatasettype #grey
	actor "Plugin Developer" as actorplugindeveloper
	boundary "Artifact\nInterface" as thingartifactinterface #grey
	control "Include The\nCode For\nA Dataset\nType In\nThe Plugin\nArtifact" as controlincludecode

	thingdatasettype <.. thingcode
	thingpluginartifact <.. thingdatasettype
	actorplugindeveloper --- thingartifactinterface
	thingcode --- controlincludecode
	thingdatasettype --- controlincludecode
	thingpluginartifact --- controlincludecode
	thingartifactinterface --> controlincludecode

@enduml