
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Radio" as thingradio
	entity "Alfred" as thingALFRED
	entity "Tv" as thingtv
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Radio\nInterface" as thingradiointerface #grey
	control "Control The\nTv" as controlcontroltv
	control "Use Alfred" as controluseALFREDcontrolcontrolradio
	control "Control Radio" as controlcontrolradio

	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingradiointerface
	thingtv --- controlcontroltv
	thingalfredinterface --> controlcontroltv
	controluseALFREDcontrolcontrolradio --> controlcontroltv
	thingALFRED --- controluseALFREDcontrolcontrolradio
	thingalfredinterface --> controluseALFREDcontrolcontrolradio
	controluseALFREDcontrolcontrolradio --> controlcontrolradio
	thingradio --- controlcontrolradio
	thingradiointerface --> controlcontrolradio

@enduml