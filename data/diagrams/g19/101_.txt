
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "People" as thingpeople
	actor "Medicalcaregiver" as actorMedicalCaregiver
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	control "Remind People" as controlremindpeopleexercise
	control "Exercise" as controlexercise
	control "Have Alfred" as controlhaveALFREDremindpeopleexercise

	actorMedicalCaregiver --- thingalfredinterface
	thingpeople --- controlremindpeopleexercise
	controlhaveALFREDremindpeopleexercise --> controlremindpeopleexercise
	thingalfredinterface --> controlremindpeopleexercise
	controlremindpeopleexercise --> controlexercise
	thingalfredinterface --> controlexercise
	thingALFRED --- controlhaveALFREDremindpeopleexercise
	thingalfredinterface --> controlhaveALFREDremindpeopleexercise

@enduml