
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Exercise" as thingexercise
	entity "New Activity" as thingnewactivity
	entity "Mission" as thingmission
	actor "Olderperson" as actorOlderPerson
	boundary "Goal" as thinggoal
	boundary "Goal\nInterface" as thinggoalinterface #grey
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Mission\nInterface" as thingmissioninterface #grey
	control "Do Exercise" as controldoexercise
	control "Go Out" as controlgo
	control "Do" as controldo #grey
	control "Have Alfred" as controlhaveALFREDgivegivegoalgoal
	control "Do New\nActivities" as controldonewactivity
	control "Give Me\nMission" as controlgivemission
	control "Give Me\nGoals Encourage" as controlgivegoalgoalgoal
	control "Give Me" as controlgive #grey

	actorOlderPerson --- thinggoal
	thinggoal --> thinggoal
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingmissioninterface
	thingexercise --- controldoexercise
	thinggoal --> controldoexercise
	controlgivegoalgoalgoal --> controldoexercise
	thinggoal --> controlgo
	controlgivegoalgoalgoal --> controlgo
	controlgivegoalgoalgoal --> controldo
	thingALFRED --- controlhaveALFREDgivegivegoalgoal
	thingalfredinterface --> controlhaveALFREDgivegivegoalgoal
	thingmission --- controlhaveALFREDgivegivegoalgoal
	thingnewactivity --- controldonewactivity
	thinggoal --> controldonewactivity
	controlgivegoalgoalgoal --> controldonewactivity
	thingmission --- controlgivemission
	thingmissioninterface --> controlgivemission
	controlhaveALFREDgivegivegoalgoal --> controlgivemission
	thinggoal --> controlgivegoalgoalgoal
	controlhaveALFREDgivegivegoalgoal --> controlgivegoalgoalgoal
	controlhaveALFREDgivegivegoalgoal --> controlgive

@enduml