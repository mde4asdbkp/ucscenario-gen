
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Tablet" as thingtablet
	entity "Computer" as thingcomputer
	entity "Phone" as thingphone
	actor "Olderperson" as actorOlderPerson
	boundary "Phone\nInterface" as thingphoneinterface #grey
	boundary "Computer\nInterface" as thingcomputerinterface #grey
	boundary "Tablet\nInterface" as thingtabletinterface #grey
	control "Have Alfred\nOn Phone" as controlhaveALFREDphone
	control "Have Alfred\nOn All\nComputers" as controlhaveALFREDcomputer
	control "Have Alfred\nOn Tablet" as controlhaveALFREDtablet

	thingcomputer <.. thingALFRED
	thingphone <.. thingALFRED
	thingtablet <.. thingALFRED
	actorOlderPerson --- thingphoneinterface
	actorOlderPerson --- thingcomputerinterface
	actorOlderPerson --- thingtabletinterface
	thingphone --- controlhaveALFREDphone
	thingphoneinterface --> controlhaveALFREDphone
	thingALFRED --- controlhaveALFREDphone
	thingALFRED --- controlhaveALFREDcomputer
	thingcomputer --- controlhaveALFREDcomputer
	thingcomputerinterface --> controlhaveALFREDcomputer
	thingtablet --- controlhaveALFREDtablet
	thingtabletinterface --> controlhaveALFREDtablet
	thingALFRED --- controlhaveALFREDtablet

@enduml