
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fall" as thingfall
	entity "Specific Contact" as thingspecificcontact
	entity "Emergency" as thingemergency
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred" as thingALFREDdetectfallOlderPerson #grey
	control "Detect Falls" as controldetectfall
	control "Have" as controlhaveALFREDdetectfallOlderPerson
	control "Send An\nEmergency To\nA Specific\nContact" as controlsendemergencyspecificcontact

	actorOlderPerson --- thingALFREDdetectfallOlderPerson
	thingfall --- controldetectfall
	controlhaveALFREDdetectfallOlderPerson --> controldetectfall
	thingALFREDdetectfallOlderPerson --> controldetectfall
	thingALFREDdetectfallOlderPerson --> controlhaveALFREDdetectfallOlderPerson
	thingemergency --- controlsendemergencyspecificcontact
	thingspecificcontact --- controlsendemergencyspecificcontact
	thingALFREDdetectfallOlderPerson --> controlsendemergencyspecificcontact
	controlhaveALFREDdetectfallOlderPerson --> controlsendemergencyspecificcontact

@enduml