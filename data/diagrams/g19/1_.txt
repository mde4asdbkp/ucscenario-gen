
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Speech Technology" as thingspeechtechnology #grey
	entity "Information" as thinginformation
	actor "Olderperson" as actorOlderPerson
	boundary "Technology\nInterface" as thingtechnologyinterface #grey
	control "Receive Information\nFrom Alfred\nBy Speech\nTechnology" as controlreceiveinformationspeechtechnology

	thingspeechtechnology <.. thingALFRED
	thingALFRED <.. thinginformation
	actorOlderPerson --- thingtechnologyinterface
	thinginformation --- controlreceiveinformationspeechtechnology
	thingALFRED --- controlreceiveinformationspeechtechnology
	thingspeechtechnology --- controlreceiveinformationspeechtechnology
	thingtechnologyinterface --> controlreceiveinformationspeechtechnology

@enduml