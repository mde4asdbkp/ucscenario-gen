
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Game" as thinggame
	entity "Imagination" as thingimagination
	actor "Olderperson" as actorOlderPerson
	boundary "Imagination\nInterface" as thingimaginationinterface #grey
	boundary "Game\nInterface" as thinggameinterface #grey
	control "Use Imagination" as controluseplaygameimagination
	control "Play Games" as controlplaygame

	actorOlderPerson --- thingimaginationinterface
	actorOlderPerson --- thinggameinterface
	thingimagination --- controluseplaygameimagination
	thingimaginationinterface --> controluseplaygameimagination
	controluseplaygameimagination --> controlplaygame
	thinggame --- controlplaygame
	thinggameinterface --> controlplaygame

@enduml