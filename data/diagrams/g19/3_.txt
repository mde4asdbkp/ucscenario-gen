
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Speech Interaction" as thingspeechinteraction #grey
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	boundary "Interaction\nInterface" as thinginteractioninterface #grey
	control "Possible Use\nAlfred With\nSpeech Interaction" as controluseALFREDspeechinteraction

	thingspeechinteraction <.. thingALFRED
	actorOlderPerson --- thinginteractioninterface
	thingALFRED --- controluseALFREDspeechinteraction
	thingspeechinteraction --- controluseALFREDspeechinteraction
	thinginteractioninterface --> controluseALFREDspeechinteraction

@enduml