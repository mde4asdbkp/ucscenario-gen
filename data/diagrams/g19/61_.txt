
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Phone" as thingphone
	entity "Alfred" as thingALFRED
	entity "Contact List" as thingcontactlist
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Phone\nInterface" as thingphoneinterface #grey
	control "Use Alfred" as controluseALFREDmaintaincontactlist
	control "Maintain Contact\nList In\nPhone" as controlmaintaincontactlist

	thingphone <.. thingcontactlist
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingphoneinterface
	thingALFRED --- controluseALFREDmaintaincontactlist
	thingalfredinterface --> controluseALFREDmaintaincontactlist
	controluseALFREDmaintaincontactlist --> controlmaintaincontactlist
	thingcontactlist --- controlmaintaincontactlist
	thingphone --- controlmaintaincontactlist
	thingphoneinterface --> controlmaintaincontactlist

@enduml