
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Room" as thingroom
	entity "Time Slot" as thingtimeslot #grey
	entity "Location" as thinglocation
	actor "Administrator" as actoradministrator
	boundary "Slot\nInterface" as thingslotinterface #grey
	boundary "Location\nInterface" as thinglocationinterface #grey
	control "Assign A\nRoom To\nA Time\nSlot" as controlassignroomtimeslot
	control "Assign A\nRoom To\nA Location" as controlassignroomlocation

	thingtimeslot <.. thingroom
	thinglocation <.. thingroom
	actoradministrator --- thingslotinterface
	actoradministrator --- thinglocationinterface
	thingroom --- controlassignroomtimeslot
	thingtimeslot --- controlassignroomtimeslot
	thingslotinterface --> controlassignroomtimeslot
	thinglocation --- controlassignroomlocation
	thinglocationinterface --> controlassignroomlocation
	thingroom --- controlassignroomlocation

@enduml