
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ead Files" as thingeadfiles #grey
	entity "Toolkit" as thingtoolkit
	entity "Archivist" as thingarchivist
	actor "Archivist" as actorarchivist
	boundary "Toolkit\nInterface" as thingtoolkitinterface #grey
	control "Import Ead\nFiles Exported\nThe Archivists'\nToolkit" as controlimporteadfiles

	thingtoolkit <.. thingeadfiles
	thingarchivist <.. thingtoolkit
	actorarchivist --- thingtoolkitinterface
	thingeadfiles --- controlimporteadfiles
	thingtoolkit --- controlimporteadfiles
	thingtoolkitinterface --> controlimporteadfiles

@enduml