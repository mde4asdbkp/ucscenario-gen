
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "System" as thingsystem
	entity "New Password" as thingnewpassword
	entity "Term Memory\nProblems" as thingtermmemoryproblems
	actor "Member" as actormember
	boundary "Problems\nInterface" as thingproblemsinterface #grey
	control "The System\nEmail Me\nA New\nPassword Have\nShort Term\nMemory Problems" as controlhavehavesystemnewpasswordshorttermmemoryproblems
	control "Have" as controlhave

	thingnewpassword <.. thingsystem
	thingtermmemoryproblems <.. thingnewpassword
	actormember --- thingproblemsinterface
	thingsystem --- controlhavehavesystemnewpasswordshorttermmemoryproblems
	thingnewpassword --- controlhavehavesystemnewpasswordshorttermmemoryproblems
	thingtermmemoryproblems --- controlhavehavesystemnewpasswordshorttermmemoryproblems
	thingproblemsinterface --> controlhavehavesystemnewpasswordshorttermmemoryproblems
	controlhavehavesystemnewpasswordshorttermmemoryproblems --> controlhave
	thingproblemsinterface --> controlhave

@enduml