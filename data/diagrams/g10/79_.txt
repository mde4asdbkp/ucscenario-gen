
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Content" as thingcontent
	actor "Site Editor" as actorsiteeditor
	boundary "Content\nInterface" as thingcontentinterface #grey
	control "Create The\nContent The\nIs Scrum\nSection" as controlcreatecontent

	actorsiteeditor --- thingcontentinterface
	thingcontent --- controlcreatecontent
	thingcontentinterface --> controlcreatecontent

@enduml