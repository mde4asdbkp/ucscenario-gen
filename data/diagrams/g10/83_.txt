
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Class" as thingclass
	entity "Pende State" as thingpendingstate
	actor "Site Admin" as actorsiteadmin
	boundary "State\nInterface" as thingstateinterface #grey
	control "View All\nClasses In\nA Pending\nState" as controlviewclasspendingstate

	thingpendingstate <.. thingclass
	actorsiteadmin --- thingstateinterface
	thingclass --- controlviewclasspendingstate
	thingpendingstate --- controlviewclasspendingstate
	thingstateinterface --> controlviewclasspendingstate

@enduml