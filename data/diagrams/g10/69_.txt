
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alliance" as thingalliance
	actor "Site Visitor" as actorsitevisitor
	control "Visit" as controlvisitknow
	control "Know" as controlknow

	controlvisitknow --> controlknow

@enduml