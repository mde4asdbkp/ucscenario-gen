
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Event" as thingevent
	entity "Course" as thingcourse
	actor "Trainer" as actortrainer
	boundary "Event\nInterface" as thingeventinterface #grey
	control "Turn A\nCourse Into\nAn Event" as controlturncourseevent

	thingcourse <.. thingevent
	thingevent <.. thingcourse
	actortrainer --- thingeventinterface
	thingeventinterface --> controlturncourseevent
	thingcourse --- controlturncourseevent
	thingevent --- controlturncourseevent

@enduml