
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Profile Page" as thingprofilepage #grey
	entity "Rating" as thingrating
	actor "Trainer" as actortrainer
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Show Rating\nUp On\nProfile Page" as controlshowratingprofilepage

	thingprofilepage <.. thingrating
	actortrainer --- thingpageinterface
	thingrating --- controlshowratingprofilepage
	thingprofilepage --- controlshowratingprofilepage
	thingpageinterface --> controlshowratingprofilepage

@enduml