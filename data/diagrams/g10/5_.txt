
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Cst" as thingCST
	entity "Site" as thingsite
	entity "Small Graphic" as thingsmallgraphic
	entity "Article" as thingarticle
	entity "Csp Status" as thingcspstatus
	actor "Practitioner" as actorpractitioner
	boundary "Article" as thingarticleshowCSTpractitioner #grey
	control "Show Cst" as controlshowCST
	control "Write An\nArticle For\nThe Site\nWith A\nSmall Graphic\nWith" as controlwritearticlesmallgraphic
	control "Show Csp\nStatus" as controlshowcspstatus
	control "Show" as controlshow #grey

	thingsite <.. thingarticle
	actorpractitioner --- thingarticleshowCSTpractitioner
	thingarticleshowCSTpractitioner --> controlshowCST
	thingCST --- controlshowCST
	controlwritearticlesmallgraphic --> controlshowCST
	thingarticle --- controlwritearticlesmallgraphic
	thingsite --- controlwritearticlesmallgraphic
	thingsmallgraphic --- controlwritearticlesmallgraphic
	thingarticleshowCSTpractitioner --> controlwritearticlesmallgraphic
	controlwritearticlesmallgraphic --> controlshowcspstatus
	thingcspstatus --- controlshowcspstatus
	thingarticleshowCSTpractitioner --> controlshowcspstatus
	thingarticleshowCSTpractitioner --> controlshow
	controlwritearticlesmallgraphic --> controlshow

@enduml