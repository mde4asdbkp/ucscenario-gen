
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Department" as thingdepartment
	entity "Responsibility" as thingresponsibility
	entity "Researcher" as thingotherresearcher
	entity "Section" as thingsection
	actor "Researcher" as actorresearcher
	boundary "Responsibility\nInterface" as thingresponsibilityinterface #grey
	control "See The\nSections On\nResponsibilities Other\nResearchers In\nDepartment" as controlseesection

	thingotherresearcher <.. thingresponsibility
	thingdepartment <.. thingotherresearcher
	thingresponsibility <.. thingsection
	actorresearcher --- thingresponsibilityinterface
	thingsection --- controlseesection
	thingresponsibility --- controlseesection
	thingresponsibilityinterface --> controlseesection

@enduml