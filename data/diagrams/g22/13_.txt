
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Pointer" as thingpointer
	entity "Metadata" as thingmetadata
	entity "Dmp" as thingDMP
	entity "System" as thingothersystem
	actor "Datum Librarian" as actordatalibrarian
	boundary "System\nInterface" as thingsysteminterface #grey
	control "Extract Pointers\nFrom The\nDmp To\nMetadata In\nOther Systems" as controlextractpointermetadata

	thingDMP <.. thingpointer
	thingothersystem <.. thingmetadata
	thingmetadata <.. thingDMP
	actordatalibrarian --- thingsysteminterface
	thingpointer --- controlextractpointermetadata
	thingDMP --- controlextractpointermetadata
	thingmetadata --- controlextractpointermetadata
	thingothersystem --- controlextractpointermetadata
	thingsysteminterface --> controlextractpointermetadata

@enduml