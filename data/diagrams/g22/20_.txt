
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Necessary Right" as thingnecessaryright
	entity "Datum" as thingdatum
	actor "Data Manager" as actordatamanager
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Document All\nNecessary Rights\nManaging The\nData" as controldocumentnecessaryrightdatum

	thingdatum <.. thingnecessaryright
	actordatamanager --- thingdatuminterface
	thingnecessaryright --- controldocumentnecessaryrightdatum
	thingdatum --- controldocumentnecessaryrightdatum
	thingdatuminterface --> controldocumentnecessaryrightdatum

@enduml