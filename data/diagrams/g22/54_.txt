
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	entity "Creation Process" as thingcreationprocess #grey
	entity "Reference" as thingreference
	entity "Documentation" as thingdocumentation
	actor "Data Manager" as actordatamanager
	boundary "Process\nInterface" as thingprocessinterface #grey
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Have References\nTo Documentation\nOf Creation\nProcess" as controlhavereferencedocumentationcreationprocess
	control "Have References\nTo Documentation\nOf The\nData" as controlhavereferencedocumentation

	thingdocumentation <.. thingreference
	thingdatum *-- thingdocumentation
	thingcreationprocess *-- thingdocumentation
	actordatamanager --- thingprocessinterface
	actordatamanager --- thingdatuminterface
	thingcreationprocess --- controlhavereferencedocumentationcreationprocess
	thingprocessinterface --> controlhavereferencedocumentationcreationprocess
	thingreference --- controlhavereferencedocumentationcreationprocess
	thingdocumentation --- controlhavereferencedocumentationcreationprocess
	thingreference --- controlhavereferencedocumentation
	thingdocumentation --- controlhavereferencedocumentation
	thingdatum --- controlhavereferencedocumentation
	thingdatuminterface --> controlhavereferencedocumentation

@enduml