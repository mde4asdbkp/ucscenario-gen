
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Packages" as thingdatapackages
	actor "Publisher" as actorpublisher
	boundary "Packages\nInterface" as thingpackagesinterface #grey
	control "Undelete The\nDeleted Data\nPackages" as controlundeletedeleteddatapackages

	actorpublisher --- thingpackagesinterface
	thingdatapackages --- controlundeletedeleteddatapackages
	thingpackagesinterface --> controlundeletedeleteddatapackages

@enduml