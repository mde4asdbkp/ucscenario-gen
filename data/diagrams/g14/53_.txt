
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Packages" as thingdatapackages #grey
	entity "Publisher" as thingpublisher
	actor "Consumer" as actorconsumer
	boundary "Packages\nInterface" as thingpackagesinterface #grey
	control "Search Among\nAll Data\nPackages A\nPublisher" as controlsearchdatapackages

	thingpublisher <.. thingdatapackages
	actorconsumer --- thingpackagesinterface
	thingdatapackages --- controlsearchdatapackages
	thingpackagesinterface --> controlsearchdatapackages

@enduml