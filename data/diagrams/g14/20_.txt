
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Cli" as thingcli
	entity "Publishing" as thingpublishing
	entity "Data Package" as thingdatapackage
	actor "Publisher" as actorpublisher
	circle "View" as thingview
	boundary "Publishing\nInterface" as thingpublishinginterface #grey
	control "Preview The\nViews Of\nThe Current\nData Package\nPrior Using\nCli To\nPublishing" as controlpreviewviewclipublishing

	thingpublishing <.. thingcli
	thingcli <.. thingdatapackage
	thingdatapackage *-- thingview
	actorpublisher --- thingpublishinginterface
	thingview --- controlpreviewviewclipublishing
	thingdatapackage --- controlpreviewviewclipublishing
	thingcli --- controlpreviewviewclipublishing
	thingpublishing --- controlpreviewviewclipublishing
	thingpublishinginterface --> controlpreviewviewclipublishing

@enduml