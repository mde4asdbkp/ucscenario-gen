
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Billing" as thingbilling
	entity "Pricing System" as thingpricingsystem
	actor "Admin" as actoradmin
	boundary "Billing\nInterface" as thingbillinginterface #grey
	boundary "System\nInterface" as thingsysteminterface #grey
	control "Have Billing" as controlhavebilling
	control "Have A\nPlan Pricing\nSystem" as controlhaveplanpricingsystem

	actoradmin --- thingbillinginterface
	actoradmin --- thingsysteminterface
	thingbilling --- controlhavebilling
	thingbillinginterface --> controlhavebilling
	thingpricingsystem --- controlhaveplanpricingsystem
	thingsysteminterface --> controlhaveplanpricingsystem

@enduml