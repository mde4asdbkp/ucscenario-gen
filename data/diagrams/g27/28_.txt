
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository System" as thingrepositorysystem
	entity "Dmp" as thingdmp
	entity "Sharing" as thingsharing
	entity "Research" as thingresearch
	entity "Researcher" as thingresearcher
	entity "Cornell" as thingcornell
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	circle "Result" as thingresult
	boundary "Dmp\nInterface" as thingdmpinterface #grey
	control "Tell The\nResearchers" as controltellresearcher
	control "Helping With\nDmps" as controlhelptellresearcherdmp

	thingsharing <.. thingrepositorysystem
	thingresult <.. thingsharing
	thingresearch *-- thingresult
	actorlibrarystaffmember --- thingdmpinterface
	thingresearcher --- controltellresearcher
	controlhelptellresearcherdmp --> controltellresearcher
	thingdmpinterface --> controltellresearcher
	thingdmp --- controlhelptellresearcherdmp
	thingdmpinterface --> controlhelptellresearcherdmp

@enduml