
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Orcid" as thingORCID
	entity "Submission" as thingsubmission
	entity "Researcherid" as thingresearcherID
	actor "Cornell ,\nFaculty Member" as actorcornellfacultymember
	boundary "Orcid\nInterface" as thingorcidinterface #grey
	boundary "Submission\nInterface" as thingsubmissioninterface #grey
	control "Associate Orcid" as controlassociateORCID
	control "Associate A\nResearcherid With\nA Submission" as controlassociateresearcherIDsubmission

	thingsubmission <.. thingresearcherID
	actorcornellfacultymember --- thingorcidinterface
	actorcornellfacultymember --- thingsubmissioninterface
	thingORCID --- controlassociateORCID
	thingorcidinterface --> controlassociateORCID
	thingresearcherID --- controlassociateresearcherIDsubmission
	thingsubmission --- controlassociateresearcherIDsubmission
	thingsubmissioninterface --> controlassociateresearcherIDsubmission

@enduml