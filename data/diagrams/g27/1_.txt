
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository" as thingrepository
	entity "Student" as thingstudent
	entity "Item" as thingitem
	actor "Faculty Member" as actorfacultymember
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	control "Direct Students\nTo An\nItem Within\nThe Repository" as controldirectstudentitem

	thingitem <.. thingstudent
	thingrepository <.. thingitem
	actorfacultymember --- thingrepositoryinterface
	thingstudent --- controldirectstudentitem
	thingitem --- controldirectstudentitem
	thingrepository --- controldirectstudentitem
	thingrepositoryinterface --> controldirectstudentitem

@enduml