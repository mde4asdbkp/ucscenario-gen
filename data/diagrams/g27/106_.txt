
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Law School" as thinglawschool #grey
	entity "Rot" as thingrot
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	circle "Scholarship" as thingscholarship
	boundary "Sustainable Link" as thingsustainablelink
	control "Link Rot" as controllinkrot
	control "Preserve The\nScholarship Of\nThe Law\nSchool Using\nSustainable Links\nNot Succumb" as controlpreservescholarshipsustainablelink

	thinglawschool *-- thingscholarship
	actorlibrarystaffmember --- thingsustainablelink
	thingrot --- controllinkrot
	controlpreservescholarshipsustainablelink --> controllinkrot
	thingsustainablelink --> controllinkrot
	thingscholarship --- controlpreservescholarshipsustainablelink
	thinglawschool --- controlpreservescholarshipsustainablelink
	thingsustainablelink --> controlpreservescholarshipsustainablelink

@enduml