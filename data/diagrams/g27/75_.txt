
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Project Site" as thingprojectsite #grey
	entity "Variety" as thingvariety
	entity "Place" as thingplace
	entity "Material" as thingmaterial
	entity "Extra Step" as thingextrastep
	entity "Directory" as thingdirectory
	entity "County" as thingowncounty
	entity "Region" as thingregion
	entity "Presentation" as thingpresentation
	entity "Document" as thingdocument
	entity "Data Set" as thingdataset
	actor "Extension Educator" as actorextensioneducator
	boundary "Variety\nInterface" as thingvarietyinterface #grey
	boundary "Document\nInterface" as thingdocumentinterface #grey
	boundary "Presentation\nInterface" as thingpresentationinterface #grey
	boundary "Place\nInterface" as thingplaceinterface #grey
	control "Have A\nPlace" as controlhaveplacestorevarietystorevarietypresentationcreate
	control "Store A\nVariety Of\nData Set" as controlstorevarietydataset
	control "Store A\nVariety Of\nDocuments" as controlstorevariety
	control "Store A\nVariety Of\nPresentation" as controlstorevarietypresentation
	control "Created" as controlcreatestorevariety

	thingdocument *-- thingvariety
	thingdataset *-- thingvariety
	thingpresentation *-- thingvariety
	thingowncounty <.. thingextrastep
	thingregion <.. thingextrastep
	thingprojectsite <.. thingextrastep
	thingdirectory <.. thingextrastep
	thingmaterial <.. thingextrastep
	actorextensioneducator --- thingvarietyinterface
	actorextensioneducator --- thingdocumentinterface
	actorextensioneducator --- thingpresentationinterface
	actorextensioneducator --- thingplaceinterface
	thingvariety --- controlhaveplacestorevarietystorevarietypresentationcreate
	thingplace --- controlhaveplacestorevarietystorevarietypresentationcreate
	thingplaceinterface --> controlhaveplacestorevarietystorevarietypresentationcreate
	thingdataset --- controlstorevarietydataset
	thingvarietyinterface --> controlstorevarietydataset
	thingvariety --- controlstorevarietydataset
	controlhaveplacestorevarietystorevarietypresentationcreate --> controlstorevarietydataset
	thingvariety --- controlstorevariety
	thingdocument --- controlstorevariety
	controlcreatestorevariety --> controlstorevariety
	thingdocumentinterface --> controlstorevariety
	controlhaveplacestorevarietystorevarietypresentationcreate --> controlstorevarietypresentation
	thingpresentation --- controlstorevarietypresentation
	thingpresentationinterface --> controlstorevarietypresentation
	thingvariety --- controlstorevarietypresentation
	thingdocumentinterface --> controlcreatestorevariety

@enduml