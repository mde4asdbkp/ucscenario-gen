
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Use ,\nRepository Shibboleth" as thingusingrepositoryshibboleth
	actor "Cornell ,\nFaculty Member" as actorcornellfacultymember
	boundary "Shibboleth\nInterface" as thingshibbolethinterface #grey
	control "Login To\nThe Using,\nRepository Shibboleth" as controlloginusingrepositoryshibboleth

	actorcornellfacultymember --- thingshibbolethinterface
	thingusingrepositoryshibboleth --- controlloginusingrepositoryshibboleth
	thingshibbolethinterface --> controlloginusingrepositoryshibboleth

@enduml