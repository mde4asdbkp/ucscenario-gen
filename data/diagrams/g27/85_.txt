
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Password" as thingpassword
	entity "Personal Account" as thingpersonalaccount
	actor "Ir" as actorir #grey
	actor "Db Administrator" as actordbadministrator
	boundary "Ability" as thingabilitychangepasswordirdbadministrator #grey
	boundary "Account\nInterface" as thingaccountinterface #grey
	boundary "Password\nInterface" as thingpasswordinterface #grey
	control "Retrieve Forgotten\nOnes" as controlretrieveforgottenone
	control "Change Passwords" as controlchangepassword
	control "Make" as controlmake
	control "Have A\nPersonal Account\nWith" as controlhavepersonalaccount

	actordbadministrator --- thingabilitychangepasswordirdbadministrator
	actorir --- thingabilitychangepasswordirdbadministrator
	actordbadministrator --- thingaccountinterface
	actorir --- thingaccountinterface
	actorir --- thingpasswordinterface
	actordbadministrator --- thingpasswordinterface
	thingaccountinterface --> controlretrieveforgottenone
	controlchangepassword --> controlretrieveforgottenone
	thingabilitychangepasswordirdbadministrator --> controlchangepassword
	thingpassword --- controlchangepassword
	controlhavepersonalaccount --> controlchangepassword
	thingpasswordinterface --> controlmake
	controlchangepassword --> controlmake
	thingpersonalaccount --- controlhavepersonalaccount
	thingabilitychangepasswordirdbadministrator --> controlhavepersonalaccount

@enduml