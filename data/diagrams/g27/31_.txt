
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Item" as thingitem
	entity "Repository" as thingrepository
	actor "Cornell ,\nFaculty Member" as actorcornellfacultymember
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	boundary "Item\nInterface" as thingiteminterface #grey
	control "Deposited In\nThe Repository" as controldepositseeitemrepository
	control "See Items" as controlseeitem

	actorcornellfacultymember --- thingrepositoryinterface
	actorcornellfacultymember --- thingiteminterface
	thingrepository --- controldepositseeitemrepository
	thingrepositoryinterface --> controldepositseeitemrepository
	controldepositseeitemrepository --> controlseeitem
	thingitem --- controlseeitem
	thingiteminterface --> controlseeitem

@enduml