
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "D File" as thingdfile
	entity "Fpds Data" as thingfpdsdata
	actor "User" as actoruser
	boundary "Data\nInterface" as thingdatainterface #grey
	control "Generate D\nFiles From\nFabs Fpds\nData" as controlgeneratedfilefabsfpdsdata
	control "Validate D\nFiles From\nFabs Fpds\nData" as controlvalidatedfilefabsfpdsdata

	thingfpdsdata <.. thingdfile
	actoruser --- thingdatainterface
	thingdfile --- controlgeneratedfilefabsfpdsdata
	thingfpdsdata --- controlgeneratedfilefabsfpdsdata
	thingdatainterface --> controlgeneratedfilefabsfpdsdata
	thingdfile --- controlvalidatedfilefabsfpdsdata
	thingdatainterface --> controlvalidatedfilefabsfpdsdata
	thingfpdsdata --- controlvalidatedfilefabsfpdsdata

@enduml