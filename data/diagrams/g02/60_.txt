
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Time" as thingtime
	entity "Update Date" as thingupdateddate
	actor "Agency User" as actoragencyuser
	boundary "Box" as thingboxagencyuser #grey
	boundary "Header Information\nBox" as thingheaderinformationboxagencyuser
	control "Show Updated\nDate" as controlshowheaderinformationboxagencyuserupdateddate
	control "Show Time" as controlshowboxagencyusertime

	actoragencyuser --- thingboxagencyuser
	actoragencyuser --- thingheaderinformationboxagencyuser
	thingheaderinformationboxagencyuser --> controlshowheaderinformationboxagencyuserupdateddate
	thingupdateddate --- controlshowheaderinformationboxagencyuserupdateddate
	thingboxagencyuser --> controlshowboxagencyusertime
	thingtime --- controlshowboxagencyusertime

@enduml