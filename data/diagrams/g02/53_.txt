
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fabs File" as thingfabsfile #grey
	entity "Schema V1.1\nHeaders" as thingschemav1.1headers #grey
	actor "Agency User" as actoragencyuser
	boundary "File\nInterface" as thingfileinterface #grey
	control "Use The\nSchema V1.1\nHeaders In\nFabs File" as controluseschemav1.1headersfabsfile

	thingfabsfile <.. thingschemav1.1headers
	actoragencyuser --- thingfileinterface
	thingschemav1.1headers --- controluseschemav1.1headersfabsfile
	thingfabsfile --- controluseschemav1.1headersfabsfile
	thingfileinterface --> controluseschemav1.1headersfabsfile

@enduml