
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fabs Errors" as thingfabserrors #grey
	entity "Error" as thingerror
	actor "Fab User" as actorfabsuser
	boundary "Error\nInterface" as thingerrorinterface #grey
	control "Represent Fabs\nErrors" as controlrepresentfabserrors
	control "Submission Errors" as controlsubmissionerrorrepresentfabserrors

	actorfabsuser --- thingerrorinterface
	thingfabserrors --- controlrepresentfabserrors
	controlsubmissionerrorrepresentfabserrors --> controlrepresentfabserrors
	thingerrorinterface --> controlrepresentfabserrors
	thingerror --- controlsubmissionerrorrepresentfabserrors
	thingerrorinterface --> controlsubmissionerrorrepresentfabserrors

@enduml