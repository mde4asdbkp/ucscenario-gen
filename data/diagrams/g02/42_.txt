
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Elements" as thingdataelements #grey
	actor "Agency User" as actoragencyuser
	boundary "Elements\nInterface" as thingelementsinterface #grey
	control "Derived Properly\nData Elements" as controlderivedataelements

	actoragencyuser --- thingelementsinterface
	thingdataelements --- controlderivedataelements
	thingelementsinterface --> controlderivedataelements

@enduml