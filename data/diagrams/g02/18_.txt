
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Content Mockups" as thingcontentmockups #grey
	actor "Broker User" as actorbrokeruser
	boundary "Mockups\nInterface" as thingmockupsinterface #grey
	control "Help" as controlhelpcreatecontentmockups
	control "Create Content\nMockups" as controlcreatecontentmockups

	actorbrokeruser --- thingmockupsinterface
	thingmockupsinterface --> controlhelpcreatecontentmockups
	controlhelpcreatecontentmockups --> controlcreatecontentmockups
	thingcontentmockups --- controlcreatecontentmockups
	thingmockupsinterface --> controlcreatecontentmockups

@enduml