
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ppopzip" as thingppopzip
	entity "Citywide" as thingcitywide
	entity "Validation" as thingvalidation
	actor "Fab User" as actorfabsuser
	boundary "Ppopzip\nInterface" as thingppopzipinterface #grey
	boundary "Validation\nInterface" as thingvalidationinterface #grey
	control "Submit A\nCitywide As\nA Ppopzip" as controlsubmitcitywideppopzip
	control "Pass Validations" as controlpassvalidation

	thingppopzip <.. thingcitywide
	actorfabsuser --- thingppopzipinterface
	actorfabsuser --- thingvalidationinterface
	thingcitywide --- controlsubmitcitywideppopzip
	thingppopzip --- controlsubmitcitywideppopzip
	thingppopzipinterface --> controlsubmitcitywideppopzip
	thingvalidation --- controlpassvalidation
	thingvalidationinterface --> controlpassvalidation

@enduml